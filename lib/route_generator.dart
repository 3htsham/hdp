import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:hdp/src/pages/hadith/hadith.dart';
import 'package:hdp/src/pages/hadith/hadith_by_category.dart';
import 'package:hdp/src/pages/languages.dart';
import 'package:hdp/src/pages/main_pages/about.dart';
import 'package:hdp/src/pages/main_pages/settings.dart';
import 'package:hdp/src/pages/pages.dart';
import 'package:hdp/src/pages/posts/post.dart';
import 'package:hdp/src/pages/splash_screen.dart';

import 'src/pages/salah/salah_settings.dart';

class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch(settings.name)
    {
      case '/Splash':
        return CupertinoPageRoute(builder: (_) => SplashScreen());
      case "/Pages":
        return CupertinoPageRoute(builder: (_) => PagesWidget(currentTab: args));
      case "/ViewHadith":
        return CupertinoPageRoute(builder: (_) => HadithWidget(argument: args as RouteArgument,));
      case "/HadithsByCategory":
        return CupertinoPageRoute(builder: (_) => HadithByCategoryWidget(argument: args as RouteArgument,));
      case "/ViewPost":
        return CupertinoPageRoute(builder: (_) => ViewPostWidget(argument: args as RouteArgument,));
      case "/Settings":
        return CupertinoPageRoute(builder: (_) => SettingsWidget());
      case "/SalahSettings":
        return CupertinoPageRoute(builder: (_) => SalahSettings());
      case "/Languages":
        return CupertinoPageRoute(builder: (_) => LanguagesWidget());
      case "/About":
        return CupertinoPageRoute(builder: (_) => AboutUsWidget());

    }
  }

}