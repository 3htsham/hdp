String IS_DARK = "isDark";
String APP_LANG = "appLang";
String CURRENT_USER = "currentUser";
String ABOUT = "about";

String HADITH_NOTIF = "hadithNotif";
String POST_NOTIF = "postNotif";
String OTHER_NOTIF = "otherNotif";

String HADITH_TOPIC = "hadithNotificationTopic";
String POST_TOPIC = "postNotificationTopic";
String CUSTOM_TOPIC = "customNotificationTopic";

String PLAY_STORE_URL = "Download  our app from PlayStore: ";

String CALCULATION_METHOD_INDEX = "calculationMethodIndex";
String ASR_CALCULATION = "asrCalculation";


