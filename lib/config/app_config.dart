import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;

class App {
  BuildContext _context;
  double _height;
  double _width;
  double _heightPadding;
  double _widthPadding;

  App(_context) {
    this._context = _context;
    MediaQueryData _queryData = MediaQuery.of(this._context);
    _height = _queryData.size.height / 100.0;
    _width = _queryData.size.width / 100.0;
    _heightPadding = _height - ((_queryData.padding.top + _queryData.padding.bottom) / 100.0);
    _widthPadding = _width - (_queryData.padding.left + _queryData.padding.right) / 100.0;
  }

  double appHeight(double v) {
    return _height * v;
  }

  double appWidth(double v) {
    return _width * v;
  }

  double appVerticalPadding(double v) {
    return _heightPadding * v;
  }

  double appHorizontalPadding(double v) {
    return _widthPadding * v;
  }
}

class Colors {
  // Color _mainColor = Color(0xFF7ee16c);
  Color _mainColor = Color(0xFF3fe14f);
  // Color _secondColor = Color(0xFF121212);
  Color _secondColor = Color(0xFF363736);
  Color _accentColor = Color(0xFF9999aa);
  // Color _scaffoldColor = Color(0xFFe2f3f6);
  Color _scaffoldColor = Color(0xFFf9fdfa);

  // Color _mainDarkColor = Color(0xFF09e3a9);
  Color _mainDarkColor = Color(0xFF00e3a6);
  // Color _secondDarkColor = Color(0xFFccccdd);
  Color _secondDarkColor = Color(0xFFf9fdfa);
  Color _accentDarkColor = Color(0xFF9999aa);
  // Color _scaffoldDarkColor = Color(0xFF121212);
  Color _scaffoldDarkColor = Color(0xFF363736);
  // Color _scaffoldDarkColor = Color(0xFF000000);

  /// Other Colors///////////////////////////////////////////
  ///Name with 2 is dark color

  Color red = Color(0xFFf27059);
  Color red2 = Color(0xFFf25c54);
  Color orange = Color(0xFFf48c06);
  Color orange2 = Color(0xFFfb8500);
  Color yellow = Color(0xFFffea00);
  Color yellow2 = Color(0xFFffdd00);
  Color turquoise = Color(0xFF72efdd);
  Color turquoise2 = Color(0xFF00b4d8);
  Color pink = Color(0xFFff5c8a);
  Color pink2 = Color(0xFFff0a54);
  Color violet = Color(0xFF9d4edd);
  Color violet2 = Color(0xFF5a189a);
  Color blue = Color(0xFF0096ff);
  Color blue2 = Color(0xFF005ce6);
  Color grey = Color(0xFFadb5bd);
  Color grey2 = Color(0xFF6c757d);
  Color green = Color(0xFF70e000);
  Color green2 = Color(0xFF38b000);

  Color cyan1 = Color(0xFF03e7a6);
  Color cyan2 = Color(0xFF00cacb);
  Color purple1 = Color(0xFFa14bfb);
  Color purple2 = Color(0xFF6e46dc);
  /////////////////////////////////////////////////////////////


  Color mainColor(double opacity) {
    return this._mainColor.withOpacity(opacity);
  }

  Color mainDarkColor(double opacity) {
    return this._mainDarkColor.withOpacity(opacity);
  }

  Color secondColor(double opacity) {
    return this._secondColor.withOpacity(opacity);
  }

  Color secondDarkColor(double opacity) {
    return this._secondDarkColor.withOpacity(opacity);
  }

  Color accentColor(double opacity) {
    return this._accentColor.withOpacity(opacity);
  }

  Color accentDarkColor(double opacity) {
    return this._accentDarkColor.withOpacity(opacity);
  }

  Color scaffoldColor(double opacity) {
    return _scaffoldColor.withOpacity(opacity);
  }

  Color scaffoldDarkColor(double opacity) {
    return _scaffoldDarkColor.withOpacity(opacity);
  }

  Gradient gradient(BuildContext context){
    Gradient appGradient = LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          mainDarkColor(1),
          mainDarkColor(0.6),
          mainColor(0.9),
        ]
    );
    return appGradient;
  }

  Gradient customGradient({AlignmentGeometry begin, AlignmentGeometry end, List<Color> colors} ){
    Gradient appGradient = LinearGradient(
        begin: begin,
        end: end,
        colors: [
          colors[0],
          colors[1],
        ]
    );
    return appGradient;
  }

  Gradient appGradient(){
    return LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [
          _mainColor,
          _mainDarkColor,
        ]
    );
  }

}