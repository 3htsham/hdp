import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:hdp/src/controllers/controller.dart';
import 'package:hdp/src/models/settings.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/route_generator.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(Controller.firebaseMessagingBackgroundHandler);
  await GlobalConfiguration().loadFromAsset("configurations");
  await FlutterDownloader.initialize();
  await Firebase.initializeApp();
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    settingsRepo.initSettings();
    // initializePackages();
    super.initState();
  }

  initializePackages() async {
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return ValueListenableBuilder(
      valueListenable: settingsRepo.setting,
      builder: (context, Settings _settings, _){
        var lang = _settings.language.value;
        return MaterialApp(
          title: "Hadith Production",
          initialRoute: "/Splash",
          onGenerateRoute: RouteGenerator.generateRoute,
          debugShowCheckedModeBanner: false,
          locale: lang,
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          // localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('en', '')),
          theme: _settings.brightness.value == Brightness.light
              ? ThemeData(
            // fontFamily: Localizations.localeOf(context).languageCode == 'ar'? 'FrutigerLTArabic' : 'Quicksand',
            // fontFamily: lang.languageCode == 'ar'? 'FrutigerLTArabic' :  'Quicksand',
            // fontFamily: 'Quicksand',
            fontFamily: 'UrduFonts',
            brightness: _settings.brightness.value,
            scaffoldBackgroundColor: config.Colors().scaffoldColor(1),
            primaryColor: config.Colors().scaffoldColor(1),
            primaryColorDark: config.Colors().scaffoldDarkColor(1),
            accentColor: config.Colors().mainColor(1),
            focusColor: config.Colors().accentColor(1),
            hintColor: config.Colors().accentColor(1),
            textTheme: TextTheme(
              headline1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w800, color: config.Colors().secondColor(1)),
              headline2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
              headline3: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w700, color: config.Colors().secondColor(1)),
              headline4: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
              headline5: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w800, color: config.Colors().secondColor(1)),
              headline6: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
              subtitle1: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w800, color: config.Colors().secondColor(1)),
              subtitle2: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
              bodyText1: TextStyle(fontSize: 14.0, color: config.Colors().secondColor(1)),
              bodyText2: TextStyle(fontSize: 12.0, color: config.Colors().secondColor(1)),
              caption: TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
            ),
          )
              :
          ThemeData(
            // fontFamily: Localizations.localeOf(context).languageCode == 'ar'? 'FrutigerLTArabic' : 'Quicksand',
            // fontFamily: lang.languageCode == 'ar'? 'FrutigerLTArabic' :  'Quicksand',
            // fontFamily: 'Quicksand',
            fontFamily: 'UrduFonts',
            brightness: _settings.brightness.value,
            scaffoldBackgroundColor: config.Colors().scaffoldDarkColor(1),
            primaryColor: config.Colors().scaffoldDarkColor(1),
            primaryColorDark: config.Colors().scaffoldColor(1),
            accentColor: config.Colors().mainDarkColor(1),
            focusColor: config.Colors().accentDarkColor(1),
            hintColor: config.Colors().accentDarkColor(1),
            textTheme: TextTheme(
              headline1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w800, color: config.Colors().mainDarkColor(1)),
              headline2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: config.Colors().mainDarkColor(1)),
              headline3: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w700, color: config.Colors().secondDarkColor(1)),
              headline4: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: config.Colors().secondDarkColor(1)),
              headline5: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w800, color: config.Colors().secondDarkColor(1)),
              headline6: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: config.Colors().secondDarkColor(1)),
              subtitle1: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w800, color: config.Colors().mainDarkColor(1)),
              subtitle2: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w600, color: config.Colors().mainDarkColor(1)),
              bodyText1: TextStyle(fontSize: 14.0, color: config.Colors().secondDarkColor(1)),
              bodyText2: TextStyle(fontSize: 12.0, color: config.Colors().secondDarkColor(1)),
              caption: TextStyle(fontSize: 12.0, color: config.Colors().accentDarkColor(1)),
            ),
          ),
        );
      },
    );
  }
}





