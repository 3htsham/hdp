import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/models/language.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;


class LanguagesWidget extends StatefulWidget {
  @override
  _LanguagesWidgetState createState() => _LanguagesWidgetState();
}

class _LanguagesWidgetState extends State<LanguagesWidget> {
  LanguagesList languagesList;

  @override
  void initState() {
    languagesList = new LanguagesList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [

              ListTile(
                leading: Icon(CupertinoIcons.chevron_right_circle_fill, color: theme.accentColor,),
                title: Text(S.of(context).app_language, style: textTheme.headline3,),
                subtitle: Text(S.of(context).select_your_preferred_language, style: textTheme.caption,),
              ),
              SizedBox(height: 10),
              ListView.separated(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                primary: false,
                itemCount: languagesList.languages.length,
                separatorBuilder: (context, index) {
                  return SizedBox(height: 10);
                },
                itemBuilder: (context, index) {
                  Language _language = languagesList.languages.elementAt(index);
                  bool selected = settingRepo.setting.value.language.value.languageCode == _language.code;

                  return InkWell(
                    onTap: () async {
                      settingRepo.changeLang(_language.code);
                      Navigator.of(context).pushNamedAndRemoveUntil('/Pages', (Route<dynamic> route) => false, arguments: 2);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor.withOpacity(0.9),
                        boxShadow: [
                          BoxShadow(
                              color: Theme.of(context).focusColor.withOpacity(0.1), blurRadius: 5, offset: Offset(0, 2)),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            alignment: AlignmentDirectional.center,
                            children: <Widget>[
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(40)),
                                ),
                                child: Icon(_language.flag),
                              ),
                              Container(
                                height: selected ? 40 : 0,
                                width: selected ? 40 : 0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(40)),
                                  color: Theme.of(context).accentColor.withOpacity(selected ? 0.85 : 0),
                                ),
                                child: Icon(
                                  Icons.check,
                                  size: selected ? 24 : 0,
                                  color: Theme.of(context).primaryColor.withOpacity(selected ? 0.85 : 0),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(width: 15),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _language.englishName,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: Theme.of(context).textTheme.subhead,
                                ),
                                Text(
                                  _language.localName,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar() {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(
        S.of(context).languages,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          CupertinoIcons.back,
          color: theme.accentColor,
        ),
      ),
    );
  }

}
