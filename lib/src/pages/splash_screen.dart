import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hdp/src/controllers/controller.dart';
import 'package:hdp/src/elements/other/background_flare.dart';
import 'package:hdp/src/elements/other/logo_loader.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends StateMVC<SplashScreen> {
  Controller _con;

  _SplashScreenState() :super(Controller()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(milliseconds: 4000), onDoneLoading);
  }

  onDoneLoading() async {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Navigator.of(context).pushNamedAndRemoveUntil('/Pages', (Route<dynamic> route) => false, arguments: 2);
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _con.scaffoldKey,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                gradient: config.Colors().gradient(context)
            ),
          ),

          Center(
            child: Opacity(
                opacity: 0.05,
                child: Image.asset("assets/icons/white_moon_logo.png", height: height * 0.3)
            ),
          ),


          Positioned(
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                LogoLoadingShimmer(text: "", showText: true,),

              ],
            ),
          ),



          Positioned(
            bottom: 0,
            left: 0,
            child: Opacity(
              opacity: 0.5,
                child: Image.asset("assets/img/masjid_nabvi.png", width: width/1.5,)
            ),
          ),


          Flare(
            color: Colors.white,
            offset: Offset(width, -height),
            bottom: -50,
            flareDuration: Duration(seconds: 15),
            left: 40,
            height: 60,
            width: 60,
          ),


          Flare(
            color: Colors.white,
            offset: Offset(width, -height),
            bottom: -50,
            flareDuration: Duration(seconds: 13),
            left: 10,
            height: 30,
            width: 30,
          ),


          Flare(
            color: Colors.white,
            offset: Offset(width-20, -height),
            bottom: -20,
            flareDuration: Duration(seconds: 8),
            left: 50,
            height: 42,
            width: 42,
          ),

        ],
      ),
    );
  }
}
