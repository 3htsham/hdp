import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/post_controller.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

class ViewPostWidget extends StatefulWidget {

  RouteArgument argument;

  ViewPostWidget({this.argument});

  @override
  _ViewPostWidgetState createState() => _ViewPostWidgetState();
}

class _ViewPostWidgetState extends StateMVC<ViewPostWidget> {

  PostController _con;

  _ViewPostWidgetState() : super(PostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.post = widget.argument.post;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    var dateTime = DateTime.fromMillisecondsSinceEpoch(_con.post.time);
    var format = DateFormat("dd/MM/yyyy");
    var datePosted = format.format(dateTime);


    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              SizedBox(height: 10),

              Container(
                margin: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    color: theme.scaffoldBackgroundColor,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: theme.focusColor,
                          blurRadius: 1,
                          spreadRadius: -1
                      ),
                    ]
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [

                      _con.post.isMedia
                          ? _con.post.isImage
                          ? Container(
                          width: size.width,
                          constraints: BoxConstraints(
                              maxHeight: size.width
                          ),
                          color: settingsRepo.setting.value.brightness.value == Brightness.dark
                              ? Colors.black
                              : Colors.white,
                          child: CachedNetworkImage(
                              imageUrl: _con.post.mediaLink,
                            placeholder: (context, value) {
                              return Image.asset(settingsRepo.setting.value.placeholder.value, fit: BoxFit.cover,);
                            },
                          )
                      )
                          :
                      // SizedBox(height: 0)
                      BetterPlayer.network(
                        _con.post.mediaLink,
                        // "https://storage.googleapis.com/hadithproduction-80938.appspot.com/hadiths/videos/1616334626138_image_picker18083158.mp4?GoogleAccessId=firebase-adminsdk-bp6ex%40hadithproduction-80938.iam.gserviceaccount.com&Expires=2524608000&Signature=CVa1%2B9JJXu0hsgA5LIAjean2X8p6iBdkAecBglP%2BOeuIh4goxNw5y40tSKuF2b5nXlUqWRyRQezUB1VCW6G5KyXGbmxkc%2BDJvuQl0lkozEt%2Bf1JyWWfXM1FGgqNXcDPBmQkK%2FZD1fYJx3YGxvPXdJBY1wGZ2VPH4wE7xnW4oIeyupZ6YMVSpaCu7ti40UjYp25yNP23CfILjwS%2FXCU%2FJGelTaavQe950%2Fdz7AGzTmMpDViRmXKUT103xgOGAPl1OiJqUtvsvqM3QTSstXouLW9G3rfVCSlsm6%2Bmx9W2zIOZzm0%2BXY7kcdh28Cxmt8ajwR55DGdWZuNDKd%2Br5PPd0kQ%3D%3D",
                        betterPlayerConfiguration: BetterPlayerConfiguration(
                            fit: BoxFit.scaleDown
                        ),
                      )
                          : SizedBox(height: 0),


                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                        child: Row(
                          children: [

                            Text(
                                datePosted ?? "",
                                style: textTheme.bodyText2.merge(TextStyle(
                                    color: textTheme.bodyText2.color.withOpacity(0.8)
                                ))
                            ),

                            Spacer(),

                          ],
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: Container(
                          width: size.width,
                          child: Text(
                            _con.post.content ?? "",
                            style: textTheme.bodyText1,
                          ),
                        ),
                      ),



                    ],
                  ),
                ),
              ),

              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar(){

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).post,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: (){
          Navigator.of(context).pop();
        },
        icon: Icon(CupertinoIcons.back, color: theme.accentColor,),
      ),

    );
  }

}
