import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/hadith_controller.dart';
import 'package:hdp/src/elements/hadith/hadith_post.dart';
import 'package:hdp/src/elements/shimmer/hadith_shimmer.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class HadithByCategoryWidget extends StatefulWidget {
  RouteArgument argument;

  HadithByCategoryWidget({this.argument});

  @override
  _HadithByCategoryWidgetState createState() => _HadithByCategoryWidgetState();
}

class _HadithByCategoryWidgetState extends StateMVC<HadithByCategoryWidget> {

  HadithController _con;

  _HadithByCategoryWidgetState() : super(HadithController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.selectedCategory = widget.argument.category;
    _con.getHadithsByCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Column(
          children: [

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
              child: _con.isLoadingHadiths
                  ? HadithShimmer()
                  : _con.hadiths.isEmpty
                  ? SizedBox(height:0)
                  : Column(
                children: [

                  ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: _con.hadiths.length,
                    itemBuilder: (context, i) {

                      return HadithPostWidget(
                        hadith: _con.hadiths[i],
                        onLinkTop: (String link) {
                          _con.launchLink(link);
                        },
                        onDownload: (){

                        },
                        onHadithTap: (){
                          Navigator.of(context).pushNamed("/ViewHadith", arguments: RouteArgument(hadith: _con.hadiths[i]));
                        },
                        onShare: (){
                          _con.shareHadith(_con.hadiths[i]);
                        },
                      );

                    },
                  ),

                  _con.lastPage != null
                      ? _con.isLoadingHadiths
                      ? HadithShimmer()
                      : Container(
                    margin:
                    EdgeInsets.symmetric(vertical: 7),
                    child: InkWell(
                      onTap: () {
                        if (!_con.isLoadingHadiths) {
                          _con.getHadithsByCategory();
                        }
                      },
                      splashColor: theme.accentColor,
                      child: Container(
                        width: size.width,
                        height: 40,
                        child: Center(
                            child: Text(
                              S.of(context).load_more,
                              style: textTheme.caption,
                            )),
                      ),
                    ),
                  )
                      : SizedBox(
                    height: 0,
                    width: 0,
                  ),
                ],
              ),

            )

          ],
        ),
      ),
    );
  }

  Widget _appbar() {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(
        _con.selectedCategory.name,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          CupertinoIcons.back,
          color: theme.accentColor,
        ),
      ),
    );
  }


}
