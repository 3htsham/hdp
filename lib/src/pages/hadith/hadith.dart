import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/hadith_controller.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/src/repositories/settings_repository.dart'
    as settingsRepo;

class HadithWidget extends StatefulWidget {
  RouteArgument argument;

  HadithWidget({this.argument});

  @override
  _HadithWidgetState createState() => _HadithWidgetState();
}

class _HadithWidgetState extends StateMVC<HadithWidget> {
  HadithController _con;

  _HadithWidgetState() : super(HadithController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.hadith = widget.argument.hadith;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    var dateTime = DateTime.fromMillisecondsSinceEpoch(_con.hadith.time);
    var format = DateFormat("dd/MM/yyyy");
    var datePosted = format.format(dateTime);

    return Scaffold(
        key: _con.scaffoldKey,
        appBar: _appbar(),
        body: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                decoration: BoxDecoration(
                    color: theme.scaffoldBackgroundColor,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: theme.accentColor,
                          blurRadius: 3,
                          spreadRadius: -1),
                    ]),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [

                      _con.hadith.isMedia
                          ? _con.hadith.isImage
                          ? Container(
                          width: size.width,
                          constraints:
                          BoxConstraints(maxHeight: size.width),
                          color: settingsRepo
                              .setting.value.brightness.value ==
                              Brightness.dark
                              ? Colors.black
                              : Colors.white,
                          child: CachedNetworkImage(
                              imageUrl: _con.hadith.mediaLink,
                            placeholder: (context, value) {
                              return Image.asset(settingsRepo.setting.value.placeholder.value, fit: BoxFit.cover,);
                            },))
                          : BetterPlayer.network(
                        _con.hadith.mediaLink,
                        // "https://storage.googleapis.com/hadithproduction-80938.appspot.com/hadiths/videos/1616334626138_image_picker18083158.mp4?GoogleAccessId=firebase-adminsdk-bp6ex%40hadithproduction-80938.iam.gserviceaccount.com&Expires=2524608000&Signature=CVa1%2B9JJXu0hsgA5LIAjean2X8p6iBdkAecBglP%2BOeuIh4goxNw5y40tSKuF2b5nXlUqWRyRQezUB1VCW6G5KyXGbmxkc%2BDJvuQl0lkozEt%2Bf1JyWWfXM1FGgqNXcDPBmQkK%2FZD1fYJx3YGxvPXdJBY1wGZ2VPH4wE7xnW4oIeyupZ6YMVSpaCu7ti40UjYp25yNP23CfILjwS%2FXCU%2FJGelTaavQe950%2Fdz7AGzTmMpDViRmXKUT103xgOGAPl1OiJqUtvsvqM3QTSstXouLW9G3rfVCSlsm6%2Bmx9W2zIOZzm0%2BXY7kcdh28Cxmt8ajwR55DGdWZuNDKd%2Br5PPd0kQ%3D%3D",
                        betterPlayerConfiguration:
                        BetterPlayerConfiguration(
                            fit: BoxFit.scaleDown),
                      )
                          : SizedBox(height: 0),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Row(
                          children: [

                            _con.hadith.isPinned
                                ? SizedBox(width: 25, child: Icon(CupertinoIcons.star_circle, color: config.Colors().pink2, size: 20,))
                                : SizedBox(width: 0, height: 0),

                            Text(
                                _con.hadith.isPinned ? S.of(context).todays_hadith : datePosted ?? "",
                                style: textTheme.bodyText2
                            ),

                            Spacer(),

                            _con.hadith.isMedia
                                ? IconButton(
                              onPressed: (){},
                              icon: Icon(
                                Icons.download_sharp,
                                size: 14,
                                color: config.Colors().green,
                              ),
                            )
                                : SizedBox(height: 0, width: 0),

                            IconButton(
                              onPressed: () {
                                _con.shareHadith(_con.hadith);
                                },
                              icon: Icon(
                                Icons.share,
                                size: 14,
                                color: config.Colors().orange,
                              ),
                            ),

                          ],
                        ),
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: size.width,
                              child: Text(
                                _con.hadith.urdu,
                                style: textTheme.bodyText1,
                                textAlign: TextAlign.right,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: size.width,
                              child: Text(
                                _con.hadith.english,
                                style: textTheme.bodyText1,
                                textAlign: TextAlign.left,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            _con.hadith.telegramLink.toString().length > 1
                                ? InkWell(
                                    onTap: () {
                                      _con.launchLink(_con.hadith.telegramLink.toString());
                                      },
                                    child: Row(
                                      children: [
                                        Transform.rotate(
                                            angle: -45,
                                            child: Icon(
                                              Icons.send,
                                              size: 14,
                                              color: config.Colors().blue,
                                            )),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          _con.hadith.telegramLink,
                                          style: textTheme.bodyText2.merge(
                                              TextStyle(
                                                  color: config.Colors().blue)),
                                        )
                                      ],
                                    ),
                                  )
                                : SizedBox(height: 0),
                            SizedBox(
                              height: 5,
                            ),
                            _con.hadith.whatsAppLink.toString().length > 1
                                ? InkWell(
                                    onTap: () {
                                      _con.launchLink(_con.hadith.whatsAppLink.toString());
                                      },
                                    child: Row(
                                      children: [
                                        Icon(
                                          CupertinoIcons.phone_circle,
                                          size: 14,
                                          color: config.Colors().green,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          _con.hadith.whatsAppLink,
                                          style: textTheme.bodyText2.merge(
                                              TextStyle(
                                                  color:
                                                      config.Colors().green)),
                                        )
                                      ],
                                    ),
                                  )
                                : SizedBox(height: 0),
                            SizedBox(
                              height: 5,
                            ),
                            _con.hadith.youtubeLink.toString().length > 1
                                ? InkWell(
                                    onTap: () {
                                      _con.launchLink(_con.hadith.youtubeLink.toString());
                                    },
                                    child: Row(
                                      children: [
                                        ImageIcon(
                                          AssetImage(
                                              "assets/icons/youtube.png"),
                                          size: 14,
                                          color: Colors.red,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          _con.hadith.youtubeLink,
                                          style: textTheme.bodyText2.merge(
                                              TextStyle(color: Colors.red)),
                                        )
                                      ],
                                    ),
                                  )
                                : SizedBox(height: 0),
                            SizedBox(
                              height: 5,
                            ),
                            _con.hadith.instagramLink.toString().length > 1
                                ? InkWell(
                                    onTap: () {
                                      _con.launchLink(_con.hadith.instagramLink.toString());
                                    },
                                    child: Row(
                                      children: [
                                        ImageIcon(
                                          AssetImage("assets/icons/insta.png"),
                                          size: 14,
                                          color: Colors.redAccent,
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          _con.hadith.instagramLink,
                                          style: textTheme.bodyText2.merge(
                                              TextStyle(
                                                  color: Colors.redAccent)),
                                        )
                                      ],
                                    ),
                                  )
                                : SizedBox(height: 0),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
              )
            ],
          ),
        )));
  }

  Widget _appbar() {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(
        S.of(context).hadith,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          CupertinoIcons.back,
          color: theme.accentColor,
        ),
      ),
    );
  }

}
