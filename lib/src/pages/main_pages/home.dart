import 'dart:ui';
import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hdp/src/controllers/home_controller.dart';
import 'package:hdp/src/elements/hadith/hadith_post.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/src/elements/banner/banner_carousel_slider.dart';
import 'package:hdp/src/elements/category/category_slider.dart';
import 'package:hdp/src/elements/hadith/hadiths_list.dart';

import '../../../generated/i18n.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends StateMVC<HomePage>
    with AutomaticKeepAliveClientMixin {

  HomeController _con;

  _HomePageState() : super(HomeController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getCategories();
    _con.getBanners();
    _con.getPinnedHadith();
    _con.getHadiths();
    _con?.checkAndGetLocationPermission();
    super.initState();
    _getSalahs();
  }

  _getSalahs(){
    Future.delayed(Duration(milliseconds: 200), (){
      _con?.getSalahTimes();
    });
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return RefreshIndicator(
      onRefresh: () async {
        _con.refresh();
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              // CachedNetworkImage(
              //   imageUrl: "https://drive.google.com/uc?id=1SD3gzJqoW5GRMDG2KJSnJYoLrRSyA1BG"
              // ),

              // BetterPlayer.network(
              //   "https://drive.google.com/uc?id=1PPirUVcZf1N6NijSoEvzFNYPQ-tcXWD9",
              //   // "https://storage.googleapis.com/hadithproduction-80938.appspot.com/hadiths/videos/1616334626138_image_picker18083158.mp4?GoogleAccessId=firebase-adminsdk-bp6ex%40hadithproduction-80938.iam.gserviceaccount.com&Expires=2524608000&Signature=CVa1%2B9JJXu0hsgA5LIAjean2X8p6iBdkAecBglP%2BOeuIh4goxNw5y40tSKuF2b5nXlUqWRyRQezUB1VCW6G5KyXGbmxkc%2BDJvuQl0lkozEt%2Bf1JyWWfXM1FGgqNXcDPBmQkK%2FZD1fYJx3YGxvPXdJBY1wGZ2VPH4wE7xnW4oIeyupZ6YMVSpaCu7ti40UjYp25yNP23CfILjwS%2FXCU%2FJGelTaavQe950%2Fdz7AGzTmMpDViRmXKUT103xgOGAPl1OiJqUtvsvqM3QTSstXouLW9G3rfVCSlsm6%2Bmx9W2zIOZzm0%2BXY7kcdh28Cxmt8ajwR55DGdWZuNDKd%2Br5PPd0kQ%3D%3D",
              //   betterPlayerConfiguration: BetterPlayerConfiguration(
              //       fit: BoxFit.scaleDown
              //   ),
              // ),

              BannerCarouselSlider(con: _con),

              _buildSalahTimesWidget(),

              CategorySlider(con: _con),

              _con.pinnedHadith != null
              ? Padding(
                padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                child: HadithPostWidget(
                  hadith: _con.pinnedHadith,
                  isPinned: true,
                  onLinkTop: (String link) {
                    _con.launchLink(link);
                  },
                  onDownload: (){
                    _con.downloadHadith(_con.pinnedHadith, !_con.pinnedHadith.isImage);
                  },
                  onHadithTap: (){
                    Navigator.of(context).pushNamed("/ViewHadith", arguments: RouteArgument(hadith: _con.pinnedHadith));
                  },
                  onShare: (){
                    _con.shareHadith(_con.pinnedHadith);
                  },
                ),
              )
              : SizedBox(height: 0, width: 0),


              HadithsList(con: _con),



            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSalahTimesWidget() {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage('assets/img/mosque_art.jpeg'),
          fit: BoxFit.fitWidth,
          alignment: Alignment.bottomCenter
        ),
        boxShadow: [
          BoxShadow(
            color: theme.primaryColorDark,
            blurRadius: 3
          )
        ]
      ),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 2),
            decoration: BoxDecoration(
              color: theme.scaffoldBackgroundColor,
              borderRadius: BorderRadius.circular(100)
            ),
            child: Text(
              S.of(context).salah_times,
              style: textTheme.subtitle2,
            ),
          ),
          SizedBox(height: 10,),

          Container(
            height: 65,
            width: size.width - 30,
            child: Row(
              children: List.generate(_con.salahTimes.length, (index) {
                var salah = _con.salahTimes[index];
                return Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Image.asset(salah.icon, height: 20, color: Colors.white,),
                      SizedBox(height: 4,),
                      FittedBox(
                        child: Text(
                          salah.name,
                          style: textTheme.bodyText1.copyWith(color: Colors.white),
                        ),
                      ),
                      // SizedBox(height: 4,),
                      FittedBox(
                        child: Text(
                          salah.time,
                          style: textTheme.bodyText2.copyWith(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          )

        ],
      ),
    );
  }

}











