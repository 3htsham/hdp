import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;


class AboutUsWidget extends StatefulWidget {
  @override
  _AboutUsWidgetState createState() => _AboutUsWidgetState();
}

class _AboutUsWidgetState extends State<AboutUsWidget> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
        appBar: _appbar(),
        body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  ListTile(
                    dense: true,
                    leading: Icon(CupertinoIcons.info_circle),
                    title: Text(S.of(context).about_us, style: textTheme.subtitle1),
                  ),

                      ListTile(
                        dense: true,
                        title: Text(settingsRepo.setting.value.about.value.about, style: textTheme.bodyText2),
                      ),

                  ListTile(
                    dense: true,
                    leading: Icon(CupertinoIcons.mail),
                    title: Text(S.of(context).email, style: textTheme.subtitle1),
                    subtitle: Text(settingsRepo.setting.value.about.value.email, style: textTheme.bodyText2),
                  ),

                  ListTile(
                    dense: true,
                    leading: Icon(CupertinoIcons.phone),
                    title: Text(S.of(context).contact, style: textTheme.subtitle1),
                    subtitle: Text(settingsRepo.setting.value.about.value.contact, style: textTheme.bodyText2),
                  ),


                  ListTile(
                    dense: true,
                    leading: Icon(CupertinoIcons.phone_circle),
                    title: Text(S.of(context).whatsapp, style: textTheme.subtitle1),
                    subtitle: Text(settingsRepo.setting.value.about.value.whatsapp, style: textTheme.bodyText2),
                  ),

                  SizedBox(height: 20),

                ]))));
  }

  Widget _appbar(){

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).about_us,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: (){
          Navigator.of(context).pop();
        },
        icon: Icon(CupertinoIcons.back, color: theme.accentColor,),
      ),

    );
  }
}
