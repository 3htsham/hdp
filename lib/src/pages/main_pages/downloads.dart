import 'dart:io';

import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/downloads_controller.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:open_file/open_file.dart';


class DownloadsWidget extends StatefulWidget {
  @override
  _DownloadsWidgetState createState() => _DownloadsWidgetState();
}

class _DownloadsWidgetState extends StateMVC<DownloadsWidget>
    with AutomaticKeepAliveClientMixin  {

  DownloadsController _con;

  _DownloadsWidgetState() : super(DownloadsController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getDownloadHadiths();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          _con.getDownloadHadiths();
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [

                _con.isGranted != null && _con.isGranted != true
                ? Align(
                 alignment: Alignment.center,
                  child: Container(
                    height: size.height,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        Spacer(),

                        Text(S.of(context).please_allow_storage_permission_to_see_downloads, style: textTheme.bodyText1, textAlign: TextAlign.center,),
                        SizedBox(height: 20),
                        Container(
                          width: 150,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: theme.accentColor,
                                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 6),
                              ),
                              onPressed: (){
                                _con.getDownloadHadiths();
                              },
                              child: Center(
                            child: Text(S.of(context).allow_permissions, style: textTheme.bodyText1,),
                          )),
                        ),

                        Spacer(),

                        Spacer(),

                      ],
                    ),
                  ),
                )

            : _con.mediaFiles.isNotEmpty
            ? GridView.count(
                crossAxisCount: 3,
                shrinkWrap: true,
                primary: false,
                childAspectRatio: 1/1,
                children: List.generate(
                _con.mediaFiles.length, (index) {

                  var fileName = _con.mediaFiles[index].substring(_con.mediaFiles[index].lastIndexOf("/")+1, _con.mediaFiles[index].length);
                  var date = FileStat.statSync(_con.mediaFiles[index]).modified;
                  var createdFormatted = DateFormat("MMM dd, yyyy");
                  var createdAt = createdFormatted.format(date);

                  return GestureDetector(
                    onTap: () {
                      OpenFile.open(_con.mediaFiles[index]);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                      decoration: BoxDecoration(
                          color: theme.scaffoldBackgroundColor,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: theme.accentColor,
                                blurRadius: 1,
                                spreadRadius: 0
                            ),
                          ],
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Stack(
                          fit: StackFit.expand,
                          children: [


                            fileName.startsWith("VID")
                                ? BetterPlayer.file(_con.mediaFiles[index], betterPlayerConfiguration: BetterPlayerConfiguration(
                              fit: BoxFit.cover,
                              placeholder: Image.asset(settingsRepo.setting.value.placeholder.value, fit: BoxFit.cover,),
                              autoPlay: false,
                              controlsConfiguration: BetterPlayerControlsConfiguration(
                                enableAudioTracks: false,
                                enableFullscreen: false,
                                enableMute: false,
                                enablePip: false,
                                enableOverflowMenu: false,
                                enablePlaybackSpeed: false,
                                enablePlayPause: false,
                                enableProgressBar: false,
                                enableProgressBarDrag: false,
                                enableProgressText: false,
                                enableQualities: false,
                                enableRetry: false,
                                enableSkips: false,
                                enableSubtitles: false,
                                showControls: false,
                                showControlsOnInitialize: false
                              )
                            ),)
                            : Image.file(File(_con.mediaFiles[index]), fit: BoxFit.cover),

                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Divider(thickness: 0,),

                                    Text(
                                      '${fileName}',
                                      style: textTheme.caption,
                                    ),


                                    Text(
                                      '${createdAt}',
                                      style: textTheme.caption.merge(TextStyle(
                                        fontSize: 9
                                      )),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
            }),
        )
                :SizedBox()

              ],
            ),
          ),
        ),
      ),
    );
  }
}
