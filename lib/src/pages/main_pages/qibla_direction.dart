import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/qibla_controller.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'dart:math' as math;

class QiblaDirectionWidget extends StatefulWidget {
  @override
  _QiblaDirectionWidgetState createState() => _QiblaDirectionWidgetState();
}

class _QiblaDirectionWidgetState extends StateMVC<QiblaDirectionWidget> {

  QiblaController _con;

  _QiblaDirectionWidgetState() : super(QiblaController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.checkLocationPermission();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dialDecoration = BoxDecoration(
      color: theme.scaffoldBackgroundColor,
        borderRadius: BorderRadius.circular(500),
      boxShadow: [
        BoxShadow(
          color: theme.focusColor,
          blurRadius: 15,
          spreadRadius: 4
        )
      ]
    );

    return Scaffold(
      key: _con.scaffoldKey,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Stack(
          children: [



            Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(top: 40),
                child: Text(
                  S.of(context).qibla_direction_may_not_work_properly,
                  style: textTheme.caption,
                  textAlign: TextAlign.center,
                ),
              ),
            ),

            Align(
              alignment: Alignment.center,
              child: _con.fetched
                  ? Stack(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                    decoration: dialDecoration,
                    child: Transform.rotate(
                      angle: (_con.compassOffset * (math.pi / 180) * -1),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(500),
                          child: Image.asset('assets/img/compass.png',)
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                    child: Transform.rotate(
                      angle: ((_con.compassOffset + _con.qiblaOffset) *
                          (math.pi / 180) *
                          -1.1),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(500),
                          child: Image.asset('assets/img/needle.png')),
                    ),
                  ),
                ],
              )
                  : Container(
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                decoration: dialDecoration,
                child: Transform.rotate(
                  angle: (1 * (math.pi / 180) * -1),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(500),
                      child: Image.asset('assets/img/compass.png')),
                ),
              )
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(top: 40, bottom: 25),
                child: !_con.serviceEnabled || (_con.permissionGranted != null && _con.permissionGranted != PermissionStatus.granted)
                    ? GestureDetector(
                  onTap: (){
                    _con.checkLocationPermission();
                  },
                  child: Text(
                    S.of(context).enable_location_services_and_allow_permission,
                    style: textTheme.caption,
                    textAlign: TextAlign.center,
                  ),
                ) : Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Text(S.of(context).accuracy, style: textTheme.headline3.merge(TextStyle(color: theme.accentColor)),),
                    SizedBox(width: 10,),
                    Text(
                      _con.accuracy == 15
                          ? S.of(context).high
                      : _con.accuracy == 30
                      ? S.of(context).medium
                      : _con.accuracy == 45
                      ? S.of(context).low
                      : _con.accuracy == -1
                      ? S.of(context).unknown
                      : "",
                      style: textTheme.headline4.merge(TextStyle(
                        fontWeight: FontWeight.w400
                      )),)

                  ],
                ),
              ),
            ),




          ],
        ),
      ),
    );
  }
}
