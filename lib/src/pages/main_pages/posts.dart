import 'package:flutter/material.dart';
import 'package:hdp/src/controllers/post_controller.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/elements/post/posts_list.dart';


class PostsWidget extends StatefulWidget {
  @override
  _PostsWidgetState createState() => _PostsWidgetState();
}

class _PostsWidgetState extends StateMVC<PostsWidget>
    with AutomaticKeepAliveClientMixin {

  PostController _con;

  _PostsWidgetState() : super(PostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getPosts();
    super.initState();
  }


  @override
  bool get wantKeepAlive => true;


  @override
  Widget build(BuildContext context) {
    super.build(context);

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return RefreshIndicator(
      onRefresh: () async {
        _con.refreshPosts();
      },
      child: Scaffold(
        key: _con.scaffoldKey,
        body: SingleChildScrollView(
          child: Column(
            children: [


              PostsListWidget(con: _con),


              SizedBox(height: 10,),


            ],
          )
        )
      ),
    );
  }
}
