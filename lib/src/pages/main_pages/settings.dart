import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/settings_controller.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends StateMVC<SettingsWidget> {

  SettingsController _con;

  _SettingsWidgetState() : super(SettingsController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    print(settingRepo.setting.value.otherNotif.value);

    return Scaffold(
      key: _con.scaffoldKey,
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Column(
            children: [

              ListTile(
                onTap: (){
                  Navigator.of(context).pushNamed("/SalahSettings");
                },
                dense: true,
                title: Text(S.of(context).salah_settings, style: textTheme.bodyText1,),
                subtitle: Text(S.of(context).change_salah_calculation_method_and_madhab, style: textTheme.caption,),
                leading: ImageIcon(
                  AssetImage("assets/icons/parying_icon.png"),
                  color: theme.primaryColorDark,
                ),
                trailing: Icon(CupertinoIcons.forward, color: theme.focusColor, size: 14,),
              ),

              ListTile(
                onTap: (){},
                dense: true,
                title: Text(S.of(context).notifications_setting, style: textTheme.bodyText1,),
                leading: Icon(
                  CupertinoIcons.bell,
                  size: 14,
                ),
              ),

              ListTile(
                onTap: (){
                  settingRepo.setting.value.hadithNotif.value ?
                  settingRepo.toggleHadithNotification(false)
                  : settingRepo.toggleHadithNotification(true);
                  _con.checkNotificationsStatus();
                },
                title: Text(S.of(context).new_hadith_notification, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    settingRepo.setting.value.hadithNotif.value ?
                    settingRepo.toggleHadithNotification(false)
                        : settingRepo.toggleHadithNotification(true);
                    _con.checkNotificationsStatus();
                  },
                  icon: Icon(
                    settingRepo.setting.value.hadithNotif.value ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ListTile(
                onTap: (){
                  settingRepo.setting.value.postNotif.value ?
                  settingRepo.togglePostNotification(false)
                      : settingRepo.togglePostNotification(true);
                  _con.checkNotificationsStatus();
                },
                title: Text(S.of(context).new_post_notification, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                    size: 14,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    settingRepo.setting.value.postNotif.value ?
                    settingRepo.togglePostNotification(false)
                        : settingRepo.togglePostNotification(true);
                    _con.checkNotificationsStatus();
                    },
                  icon: Icon(
                    settingRepo.setting.value.postNotif.value ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ListTile(
                onTap: (){
                  settingRepo.setting.value.otherNotif.value ?
                  settingRepo.toggleOtherNotification(false)
                      : settingRepo.toggleOtherNotification(true);
                  _con.checkNotificationsStatus();
                },
                title: Text(S.of(context).other_notification, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                    size: 14,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    settingRepo.setting.value.otherNotif.value ?
                    settingRepo.toggleOtherNotification(false)
                        : settingRepo.toggleOtherNotification(true);
                    _con.checkNotificationsStatus();
                  },
                  icon: Icon(
                    settingRepo.setting.value.otherNotif.value ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Divider(thickness: 0.5,),
              ),

              ListTile(
                onTap: (){
                  Navigator.of(context).pushNamed("/Languages");
                },
                dense: true,
                title: Text(S.of(context).languages, style: textTheme.bodyText1,),
                subtitle: Text(S.of(context).select_your_preferred_language, style: textTheme.caption,),
                leading: Icon(
                  CupertinoIcons.at_circle,
                ),
                trailing: Icon(CupertinoIcons.forward, color: theme.focusColor, size: 14,),
              ),

              ListTile(
                onTap: () {
                  settingRepo.toggleTheme();
                  Navigator.of(context).pushNamedAndRemoveUntil('/Pages', (Route<dynamic> route) => false, arguments: 2);
                },
                title: Text(settingRepo.setting.value.brightness.value == Brightness.dark
                    ? S.of(context).light_mode
                    : S.of(context).dark_mode, style: textTheme.bodyText1,),
                subtitle: Text(S.of(context).change_your_application_theme, style: textTheme.caption,),
                leading: Icon(
                  CupertinoIcons.brightness,
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar(){

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).settings,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: (){
          Navigator.of(context).pop();
        },
        icon: Icon(CupertinoIcons.back, color: theme.accentColor,),
      ),

    );
  }
}
