import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/settings_controller.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;

class SalahSettings extends StatefulWidget {
  const SalahSettings({Key key}) : super(key: key);

  @override
  _SalahSettingsState createState() => _SalahSettingsState();
}

class _SalahSettingsState extends StateMVC<SalahSettings> {

  SettingsController _con;

  _SalahSettingsState() : super(SettingsController()) {
    _con = controller;
  }


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    var calculationMethod = settingRepo.setting.value.salahCalculationMethod.value;
    var asrCalculationMethod = settingRepo.setting.value.salahAsrCalculation.value;

    return Scaffold(
      appBar: _appbar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [

              ListTile(
                onTap: (){},
                dense: true,
                title: Text(S.of(context).asr_calculation, style: textTheme.bodyText1,),
              ),

              ///0
              ListTile(
                onTap: (){
                  if(asrCalculationMethod != Madhab.hanafi) {
                    settingRepo.setCalculationMethod(0);
                  }
                },
                title: Text(S.of(context).hanafi, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(asrCalculationMethod != Madhab.hanafi) {
                      settingRepo.setAsrCalculation(0);
                    }
                  },
                  icon: Icon(
                    asrCalculationMethod == Madhab.hanafi ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///1
              ListTile(
                onTap: (){
                  if(asrCalculationMethod != Madhab.shafi) {
                    settingRepo.setCalculationMethod(1);
                  }
                },
                title: Text(S.of(context).shafi, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(asrCalculationMethod != Madhab.shafi) {
                      settingRepo.setAsrCalculation(1);
                    }
                  },
                  icon: Icon(
                    asrCalculationMethod == Madhab.shafi ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              Divider(
                thickness: 1,
              ),

              ListTile(
                onTap: (){},
                dense: true,
                title: Text(S.of(context).calculation_method, style: textTheme.bodyText1,),
              ),

              ///0
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.karachi) {
                    settingRepo.setCalculationMethod(0);
                  }
                },
                title: Text(S.of(context).karachi, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.karachi) {
                      settingRepo.setCalculationMethod(0);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.karachi ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///1
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.dubai) {
                    settingRepo.setCalculationMethod(1);
                  }
                },
                title: Text(S.of(context).dubai, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.dubai) {
                      settingRepo.setCalculationMethod(1);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.dubai ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///2
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.tehran) {
                    settingRepo.setCalculationMethod(2);
                  }
                },
                title: Text(S.of(context).tehran, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.tehran) {
                      settingRepo.setCalculationMethod(2);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.tehran ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///3
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.umm_al_qura) {
                    settingRepo.setCalculationMethod(3);
                  }
                },
                title: Text(S.of(context).umm_ul_qurra, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.umm_al_qura) {
                      settingRepo.setCalculationMethod(3);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.umm_al_qura ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///4
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.moon_sighting_committee) {
                    settingRepo.setCalculationMethod(4);
                  }
                },
                title: Text(S.of(context).moon_sighting_committee, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.moon_sighting_committee) {
                      settingRepo.setCalculationMethod(4);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.moon_sighting_committee ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///5
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.qatar) {
                    settingRepo.setCalculationMethod(5);
                  }
                },
                title: Text(S.of(context).qatar, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.qatar) {
                      settingRepo.setCalculationMethod(5);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.qatar ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///6
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.kuwait) {
                    settingRepo.setCalculationMethod(6);
                  }
                },
                title: Text(S.of(context).kuwait, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.kuwait) {
                      settingRepo.setCalculationMethod(6);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.kuwait ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///7
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.turkey) {
                    settingRepo.setCalculationMethod(7);
                  }
                },
                title: Text(S.of(context).turkey, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.turkey) {
                      settingRepo.setCalculationMethod(7);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.turkey ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///8
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.singapore) {
                    settingRepo.setCalculationMethod(8);
                  }
                },
                title: Text(S.of(context).singapore, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.singapore) {
                      settingRepo.setCalculationMethod(8);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.singapore ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///9
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.egyptian) {
                    settingRepo.setCalculationMethod(9);
                  }
                },
                title: Text(S.of(context).egyptian, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.egyptian) {
                      settingRepo.setCalculationMethod(9);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.egyptian ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///10
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.muslim_world_league) {
                    settingRepo.setCalculationMethod(10);
                  }
                },
                title: Text(S.of(context).muslim_world_league, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.muslim_world_league) {
                      settingRepo.setCalculationMethod(10);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.muslim_world_league ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///11
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.north_america) {
                    settingRepo.setCalculationMethod(11);
                  }
                },
                title: Text(S.of(context).north_america, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.north_america) {
                      settingRepo.setCalculationMethod(11);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.north_america ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),

              ///12
              ListTile(
                onTap: (){
                  if(calculationMethod != CalculationMethod.other) {
                    settingRepo.setCalculationMethod(12);
                  }
                },
                title: Text(S.of(context).other, style: textTheme.bodyText1,),
                leading: Opacity(
                  opacity: 0.0,
                  child: Icon(
                    CupertinoIcons.bell,
                  ),
                ),
                trailing: IconButton(
                  onPressed: (){
                    if(calculationMethod != CalculationMethod.other) {
                      settingRepo.setCalculationMethod(12);
                    }
                  },
                  icon: Icon(
                    calculationMethod == CalculationMethod.other ?
                    CupertinoIcons.check_mark_circled_solid : CupertinoIcons.circle,
                    color: theme.accentColor,),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar() {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;

    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(
        S.of(context).salah_settings,
        style: textTheme.headline5,
      ),
      centerTitle: true,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        icon: Icon(
          CupertinoIcons.back,
          color: theme.accentColor,
        ),
      ),
    );
  }

}
