import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/elements/other/drawer_widget.dart';
import 'package:hdp/src/elements/other/gradient_icon.dart';
import 'package:hdp/src/pages/main_pages/downloads.dart';
import 'package:hdp/src/pages/main_pages/home.dart';
import 'package:hdp/src/pages/main_pages/posts.dart';
import 'package:hdp/src/pages/main_pages/qibla_direction.dart';
import 'package:hdp/src/pages/main_pages/settings.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;
import 'package:permission_handler/permission_handler.dart';

class PagesWidget extends StatefulWidget {
  int currentTab;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesWidget({
    Key key,
    this.currentTab,
  }) {
    currentTab = currentTab != null ? currentTab : 1;
  }

  @override
  _PagesWidgetState createState() => _PagesWidgetState();
}

class _PagesWidgetState extends State<PagesWidget> {

  PageController _pageController;

  final List<Widget> _pages = [];
  String _title;

  @override
  initState() {
    checkStoragePermission();
    super.initState();
    _pageController = PageController();
    setState(() {
      // _pages.add(HomePage());
      _pages.add(DownloadsWidget());
      _pages.add(QiblaDirectionWidget());
      _pages.add(HomePage());
      _pages.add(PostsWidget());
    });
    Future.delayed(Duration.zero, () {
      _selectPage(widget.currentTab);
    });
  }

  void _onPageChanged(int index) {
    setState(() {
      widget.currentTab = index;
    });
    _selectTitle(index);
  }

  void _selectPage(int index) {
    setState(() {
      widget.currentTab = index;
    });
    _pageController.jumpToPage(index);
    _selectTitle(index);
  }

  void _selectTitle(int index) {
    var _newTitle = S.of(context).home;
    switch(index) {
      case 0:
        _newTitle = S.of(context).downloads;
        break;
      case 1:
        _newTitle = S.of(context).qibla;
        break;
      case 2:
        _newTitle = S.of(context).home;
        break;
      case 3:
        _newTitle = S.of(context).other_posts;
        break;
      case 4:
        _newTitle = S.of(context).settings;
        break;
      default:
        _newTitle = S.of(context).home;
    }
    setState((){
      this._title = _newTitle;
    });
  }


  checkStoragePermission() async {
    var status = await Permission.storage.status;
    if(!status.isGranted) {
      if(status.isPermanentlyDenied) {
        ///Open Settings App
      } else {
        var permission = await Permission.storage.request();
        if(!permission.isGranted) {
          //checkStoragePermission();
        }
      }
    }
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async => false,
        child: Scaffold(
          key: widget.scaffoldKey,
          appBar: AppBar(
            title: Text(_title ?? S.of(context).home, style: textTheme.headline5,),
            centerTitle: true,
            elevation: 0,
            leading: IconButton(
              onPressed: (){
                if(!widget.scaffoldKey.currentState.isDrawerOpen) {
                  widget.scaffoldKey.currentState.openDrawer();
                }
              },
              icon: GradientIcon(icon: CupertinoIcons.text_alignleft, size: 24, gradient: config.Colors().appGradient(),),
            ),
            actions: [],
          ),
          drawer: DrawerWidget(),
          body: Container(
            height: size.height,
            width: size.width,
            child: Column(
              children: [

                Expanded(
                  child: PageView(
                      children: _pages,
                      controller: _pageController,
                      onPageChanged: _onPageChanged,
                      physics: NeverScrollableScrollPhysics()
                  ),
                ),

                Container(
                  width: size.width,
                  padding: EdgeInsets.symmetric(vertical: 5),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [

                        GestureDetector(
                          onTap: () {
                            _selectPage(0);
                          },
                          child: Container(
                            decoration: BoxDecoration(),
                            child: Column(
                              children: [
                                Container(
                                    padding: EdgeInsets.all(6),
                                    decoration: BoxDecoration(
                                        color: theme.accentColor.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(100)),
                                    child: GradientIcon(
                                      // icon: CupertinoIcons.book_fill,
                                      imageIconAsset: "assets/icon_pack/download.png",
                                      size: 16,
                                      gradient: config.Colors().appGradient(),
                                    )),
                                SizedBox(height: 5),
                                Text(
                                  S.of(context).downloads,
                                  style: textTheme.caption.merge(
                                      TextStyle(color: widget.currentTab == 0 ? theme.accentColor : textTheme.caption.color)),
                                )
                              ],
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: () {_selectPage(1);},
                          child: Container(
                            decoration: BoxDecoration(),
                            child: Column(
                              children: [
                                Container(
                                    padding: EdgeInsets.all(6),
                                    decoration: BoxDecoration(
                                        color: theme.accentColor.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(100)),
                                    child: GradientIcon(
                                      // icon: CupertinoIcons.book_fill,
                                      imageIconAsset: "assets/icon_pack/qibla.png",
                                      size: 16,
                                      gradient: config.Colors().appGradient(),
                                    )),
                                SizedBox(height: 5),
                                Text(
                                  S.of(context).qibla,
                                  style: textTheme.caption.merge(
                                      TextStyle(color: widget.currentTab == 1 ? theme.accentColor : textTheme.caption.color)),
                                )
                              ],
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: () {_selectPage(2);},
                          child: Container(
                            decoration: BoxDecoration(),
                            child: Column(
                              children: [
                                Container(
                                    padding: EdgeInsets.all(7),
                                    decoration: BoxDecoration(
                                        color: theme.accentColor,
                                        borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                          color: theme.accentColor,
                                          blurRadius: 5
                                        )
                                      ]
                                    ),
                                    child: Icon(
                                      CupertinoIcons.home,
                                      size: 26,
                                      color: theme.scaffoldBackgroundColor,
                                    ),
                                ),
                                SizedBox(height: 14)
                              ],
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: () {_selectPage(3);},
                          child: Container(
                            decoration: BoxDecoration(),
                            child: Column(
                              children: [
                                Container(
                                    padding: EdgeInsets.all(6),
                                    decoration: BoxDecoration(
                                        color: theme.accentColor.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(100)),
                                    child: GradientIcon(
                                      icon: CupertinoIcons.book_fill,
                                      // imageIconAsset: "assets/icon_pack/hadith.png",
                                      size: 16,
                                      gradient: config.Colors().appGradient(),
                                    )),
                                SizedBox(height: 5),
                                Text(
                                  S.of(context).other_posts,
                                  style: textTheme.caption.merge(
                                      TextStyle(color: widget.currentTab == 3 ? theme.accentColor : textTheme.caption.color)),
                                )
                              ],
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed('/Settings');
                          },
                          child: Container(
                            decoration: BoxDecoration(),
                            child: Column(
                              children: [
                                Container(
                                    padding: EdgeInsets.all(6),
                                    decoration: BoxDecoration(
                                        color: theme.accentColor.withOpacity(0.2),
                                        borderRadius: BorderRadius.circular(100)),
                                    child: GradientIcon(
                                      // icon: CupertinoIcons.book_fill,
                                      imageIconAsset: "assets/icon_pack/setting.png",
                                      size: 16,
                                      gradient: config.Colors().appGradient(),
                                    )),
                                SizedBox(height: 5),
                                Text(
                                  S.of(context).settings,
                                  style: textTheme.caption.merge(TextStyle(
                                          color: widget.currentTab == 4 ? theme.accentColor : textTheme.caption.color
                                      )),
                                )
                              ],
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                )

              ],
            ),
          ),
        )
    );
  }
}
