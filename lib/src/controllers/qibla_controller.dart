import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:location/location.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'dart:math';
import 'package:vector_math/vector_math.dart';

class QiblaController extends ControllerMVC {

  bool serviceEnabled = false;
  Location location = new Location();
  PermissionStatus permissionGranted;
  LocationData _locationData;

  ///Kaaba Lat Lng
  ///21.422512   -    39.826186
  double compassOffset = 0.0;
  double qiblaOffset = 0.0;
  double accuracy = 0.0;
  bool fetched = false;


  GlobalKey<ScaffoldState> scaffoldKey;



  QiblaController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  checkCompass(){
    try {
      FlutterCompass.events.listen((CompassEvent value) {
        var valx = value;
        setState(() {
          compassOffset = value.heading;
          accuracy = value.accuracy;
          fetched = true;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  checkLocationPermission() async {
    serviceEnabled = await location.serviceEnabled();
    if(!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }
    permissionGranted = await location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _locationData = await location.getLocation();
    qiblaOffset = getOffsetFromNorth(_locationData.latitude, _locationData.longitude, 21.422512, 39.826186);
    checkCompass();
  }


  double getOffsetFromNorth(double currentLatitude, double currentLongitude,
      double targetLatitude, double targetLongitude) {
    var la_rad = radians(currentLatitude);
    var lo_rad = radians(currentLongitude);

    var de_la = radians(targetLatitude);
    var de_lo = radians(targetLongitude);

    var toDegrees = degrees(atan(sin(de_lo - lo_rad) /
        ((cos(la_rad) * tan(de_la)) - (sin(la_rad) * cos(de_lo - lo_rad)))));
    if (la_rad > de_la) {
      if ((lo_rad > de_lo || lo_rad < radians(-180.0) + de_lo) &&
          toDegrees > 0.0 &&
          toDegrees <= 90.0) {
        toDegrees += 180.0;
      } else if (lo_rad <= de_lo &&
          lo_rad >= radians(-180.0) + de_lo &&
          toDegrees > -90.0 &&
          toDegrees < 0.0) {
        toDegrees += 180.0;
      }
    }
    if (la_rad < de_la) {
      if ((lo_rad > de_lo || lo_rad < radians(-180.0) + de_lo) &&
          toDegrees > 0.0 &&
          toDegrees < 90.0) {
        toDegrees += 180.0;
      }
      if (lo_rad <= de_lo &&
          lo_rad >= radians(-180.0) + de_lo &&
          toDegrees > -90.0 &&
          toDegrees <= 0.0) {
        toDegrees += 180.0;
      }
    }
    return toDegrees;
  }


}