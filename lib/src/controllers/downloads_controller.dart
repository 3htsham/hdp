import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';

class DownloadsController extends ControllerMVC {

  bool isGranted = false;

  List<String> mediaFiles = [];


  GlobalKey<ScaffoldState> scaffoldKey;

  DownloadsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }



  getDownloadHadiths() async {
    bool isGrantedOrNot = await checkStoragePermission();
    setState((){
      this.isGranted = isGrantedOrNot ?? false;
    });
    if (isGrantedOrNot != null && isGrantedOrNot) {
      this.mediaFiles.clear();
      var path = await _findLocalPath();
      if (Platform.isAndroid) {
        List<String> paths = path.split("/");
        String newPath = "";
        for (int x = 1; x < paths.length; x++) {
          String folder = paths[x];
          if (folder != "Android") {
            newPath += "/" + folder;
          } else {
            break;
          }
          path = newPath;
        }
      }
      String _localPath = path +
          Platform.pathSeparator +
          "HadithProduction" +
          Platform.pathSeparator;
      print("_localPath: " + _localPath);

      if (_localPath.endsWith("/")) {
        _localPath = _localPath.substring(0, _localPath.length - 1);
        print("Adjusted localPath: " +
            _localPath.substring(0, _localPath.length - 1));
      }
      final savedDir = Directory(_localPath + Platform.pathSeparator);
      bool hasExisted = await savedDir.exists();
      if (!hasExisted) {
        await savedDir.create(recursive: true);
      }
      ///Get Downloaded files now
      Directory directory = Directory(_localPath);
      var downloadedFiles = directory.listSync();

      downloadedFiles.sort((a, b) => FileStat.statSync(b.path).modified.compareTo(FileStat.statSync(a.path).modified));

      for(int i =0; i<downloadedFiles.length; i++) {
        var filename = downloadedFiles[i].path.substring(downloadedFiles[i].path.lastIndexOf("/")+1, downloadedFiles[i].path.length);
        if(filename.startsWith("IMG") || filename.startsWith("VID")) {
          setState((){
            this.mediaFiles.add(downloadedFiles[i].path);
          });
        }
      }
      print("Getting files: ${this.mediaFiles.length} __ ${downloadedFiles.length}");
    }
  }

  Future<String> _findLocalPath() async {
    final directory = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }
  Future<bool> checkStoragePermission() async {
    var status = await Permission.storage.status;
    if(!status.isGranted) {
      if(status.isPermanentlyDenied) {
        ///Open Settings App
        await openAppSettings();
        return false;
      } else {
        var permission = await Permission.storage.request();
        if(permission.isGranted) {
          //checkStoragePermission();
          return true;
        } else if(permission.isPermanentlyDenied) {
          ///Open Settings App
          await openAppSettings();
          return false;
        }
      }
    } else if(status.isGranted) {
      return true;
    } else {
      return false;
    }
  }

}