import 'dart:io';
import 'dart:math';
import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:hdp/config/strings.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/helpers/ui_helper.dart';
import 'package:hdp/src/models/banner.dart';
import 'package:hdp/src/models/category.dart';
import 'package:hdp/src/models/hadith.dart';
import 'package:hdp/src/models/salah.dart';
import 'package:intl/intl.dart';
import 'package:media_scanner_scan_file/media_scanner_scan_file.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/banner_repository.dart' as bannerRepo;
import 'package:hdp/src/repositories/category_repository.dart' as catRepo;
import 'package:hdp/src/repositories/hadith_repository.dart' as hadithRepo;
import 'package:hdp/config/app_config.dart' as config;
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:location/location.dart' as l;

class HomeController extends ControllerMVC {
  List<Color> colors = [
    config.Colors().orange2,
    config.Colors().yellow2,
    config.Colors().turquoise2,
    config.Colors().pink2,
    config.Colors().violet2,
    config.Colors().blue2,
    config.Colors().cyan2
  ];

  List<Salah> salahTimes = [];

  bool isLoadingBanners = false;
  List<MyBanner> banners = <MyBanner>[];

  bool isLoadingCategories = false;
  List<Category> categories = <Category>[];

  Hadith pinnedHadith;
  bool isLoadingHadiths = false;
  List<Hadith> hadiths = <Hadith>[];
  var lastPage;
  var newLastPage;
  bool isComplete = false;
  bool loadingNewHadiths = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  HomeController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getSalahTimes(){
    setState(() {
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).fajr, icon: 'assets/icons/sun_rise_set_icon.png', time: "04:24 AM"));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).duhr, icon: 'assets/icons/sun_icon.png', time: "12:31 PM"));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).asr, icon: 'assets/icons/sun_rise_set_icon.png', time: "03:53 PM"));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).maghrib, icon: 'assets/icons/moon_icon.png', time: "06:36 PM"));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).isha, icon: 'assets/icons/moon_star_icon.png', time: "08:28 PM"));
    });
  }

  getBanners() async {
    setState(() {
      this.banners.clear();
      isLoadingBanners = true;
    });
    Stream<MyBanner> stream = await bannerRepo.getBanners();
    stream.listen((event) {
      setState(() {
        isLoadingBanners = false;
      });
      if (event != null) {
        setState(() {
          this.banners.add(event);
        });
      }
    }, onError: (e) {
      setState(() {
        isLoadingBanners = false;
      });
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(S
              .of(scaffoldKey.currentContext)
              .verify_your_internet_connection)));
    }, onDone: () {
      setState(() {
        isLoadingBanners = false;
      });});
  }

  getCategories() async {
    setState((){isLoadingCategories = true;});
    Stream<Category> stream = await catRepo.getCategories();
    stream.listen((event) {
      setState((){isLoadingCategories = false;});
      if (event != null) {
        var ran = Random();
        var index = ran.nextInt(colors.length);
        event.color = colors[index];
        setState(() {
          this.categories.add(event);
        });
      }
    }, onError: (e){
      setState(() {
        this.isLoadingCategories = false;
      });
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(S
              .of(scaffoldKey.currentContext)
              .verify_your_internet_connection)));
    }, onDone: (){
      setState(() {
        isLoadingBanners = false;
      });});
  }

  getHadiths() async {
    if(!this.isComplete) {
      setState(() {this.isLoadingHadiths = this.lastPage == null; this.loadingNewHadiths = true;});
      Stream<Hadith> stream = await hadithRepo.getAllHadiths(
          lastPage: this.lastPage ?? null);
      stream.listen((event) {
        setState(() {this.isLoadingHadiths = false; this.loadingNewHadiths = false;});
        if (event != null) {
          setState(() {
            this.newLastPage = event.lastPage;
          });
          if(this.pinnedHadith != null) {
            if(this.pinnedHadith.id != event.id) {
              setState((){
                this.hadiths.add(event);
              });
            }
          }
        }
      },
          onError: (e) {
            setState(() {this.isLoadingHadiths = false; this.lastPage = null; this.loadingNewHadiths = false;});
            print(e);
          },
          onDone: () {
            print("Done");
            if(this.lastPage == this.newLastPage) {
              setState(() {
                this.lastPage = null;
                this.isComplete = true;
                this.loadingNewHadiths = false;
                this.isLoadingHadiths = false;
              });
            } else {
              setState(() {
                this.lastPage = this.newLastPage;
                this.isComplete = false;
                this.loadingNewHadiths = false;
                this.isLoadingHadiths = false;
              });
            }
          });
    }
  }

  getPinnedHadith() async {
    hadithRepo.getPinned().then((value) {
      if (value != null && value.id != null) {
        if(this.hadiths != null && this.hadiths.length >0) {
          for(int i = 0; i<this.hadiths.length; i++) {
            if(this.hadiths[i].id == value.id) {
              this.hadiths.removeAt(i);
            }
          }
        }
        setState((){
          pinnedHadith = value;
        });
      }
    }, onError: (e){
      print(e);
    });
  }

  getSalahTimesByLocation() async {
    l.LocationData data = await l.Location.instance.getLocation();
    final coordinates = Coordinates(data.latitude ?? 23.9088, data.longitude ?? 89.1220);
    final params = settingsRepo.setting.value.salahCalculationMethod?.value?.getParameters() ?? CalculationMethod.karachi.getParameters();
    params.madhab = settingsRepo.setting.value.salahAsrCalculation?.value ?? Madhab.hanafi;
    final prayerTimes = PrayerTimes.today(coordinates, params);
    print(prayerTimes.fajr.timeZoneName);
    print(DateFormat.jm().format(prayerTimes.fajr));
    print(DateFormat.jm().format(prayerTimes.dhuhr));
    print(DateFormat.jm().format(prayerTimes.asr));
    print(DateFormat.jm().format(prayerTimes.maghrib));
    print(DateFormat.jm().format(prayerTimes.isha));
    salahTimes.clear();
    setState(() {
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).fajr, icon: 'assets/icons/sun_rise_set_icon.png', time: DateFormat.jm().format(prayerTimes.fajr)));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).duhr, icon: 'assets/icons/sun_icon.png', time: DateFormat.jm().format(prayerTimes.dhuhr)));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).asr, icon: 'assets/icons/sun_rise_set_icon.png', time: DateFormat.jm().format(prayerTimes.asr)));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).maghrib, icon: 'assets/icons/moon_icon.png', time: DateFormat.jm().format(prayerTimes.maghrib)));
      salahTimes.add(Salah(name: S.of(this.scaffoldKey.currentContext).isha, icon: 'assets/icons/moon_star_icon.png', time: DateFormat.jm().format(prayerTimes.isha)));
    });
  }

  checkAndGetLocationPermission() async {
    l.PermissionStatus status = await l.Location.instance.hasPermission();
    if(status == l.PermissionStatus.granted) {
      if(!await l.Location.instance.requestService()) {
        UIHelper.showAlertDialog(this.scaffoldKey.currentContext, S.of(this.scaffoldKey.currentContext).permission,
            S.of(this.scaffoldKey.currentContext).please_make_sure_location_services_are_on_to_correctly_find_the_salah_times, () {
              Navigator.of(this.scaffoldKey.currentContext).pop();
            });
      } else {
        getSalahTimesByLocation();
      }
    } else if (status == l.PermissionStatus.deniedForever) {
      UIHelper.showAlertDialog(this.scaffoldKey.currentContext, S.of(this.scaffoldKey.currentContext).permission,
          S.of(this.scaffoldKey.currentContext).please_make_sure_location_permission_is_allowed_and_location_services_are_on_to_correctly_find_the_Salah_times, () {
            Navigator.of(this.scaffoldKey.currentContext).pop();
          });
    } else {
      await l.Location.instance.requestPermission();
      await l.Location.instance.requestService();
      getSalahTimesByLocation();
    }
  }

  refresh() {
    setState((){
      lastPage = null;
      newLastPage = null;
      isComplete = false;
      pinnedHadith = null;
      banners.clear();
      categories.clear();
      hadiths.clear();
    });
    getCategories();
    getBanners();
    getPinnedHadith();
    getHadiths();
  }


  shareHadith(Hadith _hadith) async {
    String textToShare = _hadith.urdu + "\n\n" + _hadith.english;
    if(settingsRepo.setting.value.about.value.playStoreUrl != null && settingsRepo.setting.value.about.value.playStoreUrl.length > 0) {
      textToShare += "\n\n" + PLAY_STORE_URL + "\n\n" + settingsRepo.setting.value.about.value.playStoreUrl;
    }
    Share.share(textToShare);
  }

  launchLink(String link) async {
    if(await canLaunch(link)) {
      launch(link);
    }
  }

  downloadHadith(Hadith _hadith, bool isVideo) async {
    if (await checkStoragePermission()) {
      var path = await _findLocalPath();
      if (Platform.isAndroid) {
        List<String> paths = path.split("/");
        String newPath = "";
        for (int x = 1; x < paths.length; x++) {
          String folder = paths[x];
          if (folder != "Android") {
            newPath += "/" + folder;
          } else {
            break;
          }
          path = newPath;
        }
      }

      Random rand = Random();
      int randomInt = rand.nextInt(100000);
      String filename = "";
      String ext = _hadith.fileName.toString().substring(
          _hadith.fileName.toString().lastIndexOf("."),
          _hadith.fileName.toString().length);
      String _localPath = path +
          Platform.pathSeparator +
          "HadithProduction" +
          Platform.pathSeparator;
      print("_localPath: " + _localPath);

      if (_localPath.endsWith("/")) {
        _localPath = _localPath.substring(0, _localPath.length - 1);
        print("Adjusted localPath: " +
            _localPath.substring(0, _localPath.length - 1));
      }

      if (isVideo) {
        filename += "VID_";
      } else {
        filename += "IMG_";
      }
      filename += "$randomInt$ext";
      print("Filename: " + filename);
      final savedDir = Directory(_localPath + Platform.pathSeparator);
      bool hasExisted = await savedDir.exists();
      if (!hasExisted) {
        await savedDir.create(recursive: true);
      }

      try {
        final taskId = await FlutterDownloader.enqueue(
          url: _hadith.mediaLink,
          savedDir: _localPath,
          fileName: filename,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
              true, // click on notification to open downloaded file (for Android)
        );

        await MediaScannerScanFile.scanFile(_localPath + Platform.pathSeparator+filename);

        print("taskId: " + taskId);
      } catch (e) {
        print("Error downloading file: " + e);
      }
    }
  }
  Future<String> _findLocalPath() async {
    final directory = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }
  Future<bool> checkStoragePermission() async {
    var status = await Permission.storage.status;
    if(!status.isGranted) {
      if(status.isPermanentlyDenied) {
        ///Open Settings App
        await openAppSettings();
        return false;
      } else {
        var permission = await Permission.storage.request();
        if(permission.isGranted) {
          //checkStoragePermission();
          return true;
        } else if(permission.isPermanentlyDenied) {
          ///Open Settings App
          await openAppSettings();
          return false;
          return false;
        }
      }
    } else if(status.isGranted) {
      return true;
    } else {
      return false;
    }
  }


}