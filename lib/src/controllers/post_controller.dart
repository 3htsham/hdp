import 'dart:math';
import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/config/strings.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/models/post.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/post_repository.dart' as repo;

class PostController extends ControllerMVC {

  Post post = Post();

  List<Post> posts = <Post>[];
  var lastPage;
  var newLastPage;
  bool isComplete = false;
  bool isLoading = false;
  bool isLoadingPosts = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  PostController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getPosts() async {
    if(!this.isComplete) {
      setState(() {this.isLoading = true; isLoadingPosts = this.lastPage == null; });
      Stream<Post> stream = await repo.getAllPosts(lastPage: this.lastPage ?? null);
      stream.listen((event) {
        setState(() {this.isLoading = false; isLoadingPosts = false;});
        if (event != null) {
          setState(() {
            this.posts.add(event);
            this.newLastPage = event.lastPage;
          });
        }
      },
          onError: (e) {
            setState(() {this.isLoading = false; this.lastPage = null; isLoadingPosts = false;});
            print(e);
          },
          onDone: () {
            print("Done");
            if(this.lastPage == this.newLastPage) {
              setState(() {
                this.lastPage = null;
                this.isComplete = true;
                isLoadingPosts = false;
              });
            } else {
              setState(() {
                this.lastPage = this.newLastPage;
                this.isComplete = false;
                isLoadingPosts = false;
              });
            }
          });
    }
  }

  refreshPosts() {
    setState((){
      this.isComplete = false;
      isComplete = false;
      isLoading = false;
      isLoadingPosts = false;
      lastPage = null;
      newLastPage = null;
      this.posts.clear();
    });
    this.getPosts();
  }

}