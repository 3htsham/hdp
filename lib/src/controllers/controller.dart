import 'dart:math';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hdp/config/strings.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;



class Controller extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;

  Controller() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    initFCM();
  }



  void initFCM() async {

    FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

    if(settingRepo.setting.value.hadithNotif.value) {
      await _firebaseMessaging.subscribeToTopic(HADITH_TOPIC);
    }
    if(settingRepo.setting.value.otherNotif.value) {
      await _firebaseMessaging.subscribeToTopic(CUSTOM_TOPIC);
    }
    if(settingRepo.setting.value.postNotif.value) {
      await _firebaseMessaging.subscribeToTopic(POST_TOPIC);
    }

    _firebaseMessaging.requestPermission(alert: true);

    FirebaseMessaging.onMessage.listen((event) {
      onMessage(event.data);
    });

  }


  static void onMessage(Map<String, dynamic> message) async {

    print("onMessage: " + message.toString());

    var initializationSettingsAndroid = AndroidInitializationSettings('noti_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);


    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );

    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: null);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    Random rand = Random();
    var _id = rand.nextInt(100000);

    flutterLocalNotificationsPlugin.show(
        0,
        message['title'],
        message['body'],
        NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              // icon: android?.smallIcon,
              // other properties...
            ),
            iOS: IOSNotificationDetails(
                badgeNumber: 0,
                presentAlert: true,
                presentSound: true,
                presentBadge: true
            )
        ));

  }

  static Future<dynamic> onDidReceiveLocalNotification(int id, String a, String b, String c) {

  }

  static Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
    // If you're going to use other Firebase services in the background, such as Firestore,
    // make sure you call `initializeApp` before using other Firebase services.
    await Firebase.initializeApp();

    print("Handling a background message: ${message.data}");

    Controller.onMessage(message.data);

  }

}