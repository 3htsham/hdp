import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/config/strings.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;
import 'package:shared_preferences/shared_preferences.dart';

class SettingsController extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;

  SettingsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  checkNotificationsStatus() async {

    FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(prefs.get(HADITH_NOTIF) ?? true) {
      await _firebaseMessaging.subscribeToTopic(HADITH_TOPIC);
    } else {
      await _firebaseMessaging.unsubscribeFromTopic(HADITH_TOPIC);
    }
    if(prefs.get(POST_NOTIF) ?? true) {
      await _firebaseMessaging.subscribeToTopic(POST_TOPIC);
    } else {
      await _firebaseMessaging.unsubscribeFromTopic(POST_TOPIC);
    }
    if(prefs.get(OTHER_NOTIF) ?? true) {
      await _firebaseMessaging.subscribeToTopic(CUSTOM_TOPIC);
    } else {
      await _firebaseMessaging.unsubscribeFromTopic(CUSTOM_TOPIC);
    }

  }

}