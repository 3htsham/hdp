import 'dart:io';
import 'dart:math';
import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:hdp/config/strings.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/models/category.dart';
import 'package:hdp/src/models/hadith.dart';
import 'package:image_picker/image_picker.dart';
import 'package:media_scanner_scan_file/media_scanner_scan_file.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:hdp/src/repositories/category_repository.dart' as catRepo;
import 'package:hdp/src/repositories/hadith_repository.dart' as repo;
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:path_provider/path_provider.dart';

class HadithController extends ControllerMVC {

  List<Category> categories = <Category>[];
  List<DropdownMenuItem<Category>> catDropdownMenuItems;
  Category selectedCategory;
  bool isLoading = false;

  Hadith hadith = Hadith();
  bool isEditing= false;

  bool notifyUsers = false;

  bool isLoadingHadiths = false;
  List<Hadith> hadiths = <Hadith>[];
  var lastPage;
  var newLastPage;
  bool isComplete = false;
  bool loadingNewHadiths = false;

  BetterPlayerController betterPlayerController;
  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> formKey;

  HadithController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.formKey = new GlobalKey<FormState>();
  }

  getHadiths() async {
    if(!this.isComplete) {
      Stream<Hadith> stream = await repo.getAllHadiths(
          lastPage: this.lastPage ?? null);
      setState(() {this.isLoading = true;});
      stream.listen((event) {
        setState(() {this.isLoading = false;});
        if (event != null) {
          setState(() {
            this.hadiths.add(event);
            this.newLastPage = event.lastPage;
          });
        }
      },
          onError: (e) {
            setState(() {this.isLoading = false; this.lastPage = null;});
        print(e);
          },
          onDone: () {
        print("Done");
        if(this.lastPage == this.newLastPage) {
          setState(() {
            this.lastPage = null;
            this.isComplete = true;
          });
        } else {
          setState(() {
            this.lastPage = this.newLastPage;
            this.isComplete = false;
          });
        }
          });
    }
  }

  getHadithsByCategory() async {
    if(!this.isComplete) {
      setState(() {this.isLoadingHadiths = this.lastPage == null; this.loadingNewHadiths = true;});
      Stream<Hadith> stream = await repo.getHadithsByCategory( this.selectedCategory.id,
          lastPage: this.lastPage ?? null);
      stream.listen((event) {
        setState(() {this.isLoadingHadiths = false; this.loadingNewHadiths = false;});
        if (event != null) {
          setState(() {
            this.hadiths.add(event);
            this.newLastPage = event.lastPage;
          });
        }
      },
          onError: (e) {
            setState(() {this.isLoadingHadiths = false; this.lastPage = null; this.loadingNewHadiths = false;});
            print(e);
          },
          onDone: () {
            print("Done");
            if(this.lastPage == this.newLastPage) {
              setState(() {
                this.lastPage = null;
                this.isComplete = true;
                this.loadingNewHadiths = false;
                this.isLoadingHadiths = false;
              });
            } else {
              setState(() {
                this.lastPage = this.newLastPage;
                this.isComplete = false;
                this.loadingNewHadiths = false;
                this.isLoadingHadiths = false;
              });
            }
          });
    }
  }

  shareHadith(Hadith _hadith) async {
    String textToShare = _hadith.urdu + "\n\n" + _hadith.english;
    if(settingsRepo.setting.value.about.value.playStoreUrl != null && settingsRepo.setting.value.about.value.playStoreUrl.length > 0) {
      textToShare += "\n\n" + PLAY_STORE_URL + "\n\n" + settingsRepo.setting.value.about.value.playStoreUrl;
    }
    Share.share(textToShare);
  }

  launchLink(String link) async {
    if(await canLaunch(link)) {
      launch(link);
    }
  }

  downloadHadith(Hadith _hadith, bool isVideo) async {
    if (await checkStoragePermission()) {
      var path = await _findLocalPath();
      if (Platform.isAndroid) {
        List<String> paths = path.split("/");
        String newPath = "";
        for (int x = 1; x < paths.length; x++) {
          String folder = paths[x];
          if (folder != "Android") {
            newPath += "/" + folder;
          } else {
            break;
          }
          path = newPath;
        }
      }

      Random rand = Random();
      int randomInt = rand.nextInt(100000);
      String filename = "";
      String ext = _hadith.fileName.toString().substring(
          _hadith.fileName.toString().lastIndexOf("."),
          _hadith.fileName.toString().length);
      String _localPath = path +
          Platform.pathSeparator +
          "HadithProduction" +
          Platform.pathSeparator;
      print("_localPath: " + _localPath);

      if (_localPath.endsWith("/")) {
        _localPath = _localPath.substring(0, _localPath.length - 1);
        print("Adjusted localPath: " +
            _localPath.substring(0, _localPath.length - 1));
      }

      if (isVideo) {
        filename += "VID_";
      } else {
        filename += "IMG_";
      }
      filename += "$randomInt$ext";
      print("Filename: " + filename);
      final savedDir = Directory(_localPath + Platform.pathSeparator);
      bool hasExisted = await savedDir.exists();
      if (!hasExisted) {
        await savedDir.create(recursive: true);
      }

      try {
        final taskId = await FlutterDownloader.enqueue(
          url: _hadith.mediaLink,
          savedDir: _localPath,
          fileName: filename,
          showNotification: true,
          // show download progress in status bar (for Android)
          openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
        );

        await MediaScannerScanFile.scanFile(_localPath + Platform.pathSeparator+filename);

        print("taskId: " + taskId);
      } catch (e) {
        print("Error downloading file: " + e);
      }
    }
  }
  Future<String> _findLocalPath() async {
    final directory = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }
  Future<bool> checkStoragePermission() async {
    var status = await Permission.storage.status;
    if(!status.isGranted) {
      if(status.isPermanentlyDenied) {
        ///Open Settings App
        await openAppSettings();
        return false;
      } else {
        var permission = await Permission.storage.request();
        if(permission.isGranted) {
          //checkStoragePermission();
          return true;
        } else if(permission.isPermanentlyDenied) {
          ///Open Settings App
          await openAppSettings();
          return false;
          return false;
        }
      }
    } else if(status.isGranted) {
      return true;
    } else {
      return false;
    }
  }

}