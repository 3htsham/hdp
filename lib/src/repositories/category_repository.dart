import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:hdp/src/helpers/helper.dart';
import 'package:hdp/src/models/category.dart';
import 'package:hdp/src/models/user.dart';
import 'package:http_parser/http_parser.dart' as parser;
import 'package:http/http.dart' as http;
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

Future<Stream<Category>> getCategories() async {
  User currentUser = settingsRepo.setting.value.currentUser.value;
  final String apiToken = "?apiToken=${currentUser.apiToken}";
  final String url = '${GlobalConfiguration().getString('api_base_url')}category';
  final client = new http.Client();
  final streamedRest =await client.send(http.Request('get', Uri.parse(url)));
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getCategoriesData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Category.fromJson(data);
      });
}