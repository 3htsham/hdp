import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:hdp/config/strings.dart';
import 'package:hdp/src/helpers/helper.dart';
import 'package:hdp/src/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart' as parser;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

User currentUser;

Future<User> login(User _user) async {
  // final String url = '${GlobalConfiguration().getString('api_base_url')}users/login';
  final String url = '${GlobalConfiguration().getString('base_url')}users/login';
  final client = new http.Client();
  final response = await client.post(
    Uri.parse(url),
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(_user.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['user'] != null) {
      setCurrentUser(body);
      currentUser = User.fromJson(json.decode(body)['user']);
      settingsRepo.setting.value.currentUser.value = currentUser;
      settingsRepo.setting.notifyListeners();
    }
  } else {
    print(response.body);
  }
  return currentUser;
}


void setCurrentUser(jsonString) async {
  if (json.decode(jsonString)['user'] != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(CURRENT_USER, json.encode(json.decode(jsonString)['user']));
  }
}

Future<User> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(CURRENT_USER)) {
    currentUser = User.fromJson(json.decode(await prefs.get(CURRENT_USER)));
  }
  return currentUser;
}

Future<void> logout() async {
  currentUser = new User();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove(CURRENT_USER);
  settingsRepo.setting.value.currentUser.value = User();
  settingsRepo.setting.notifyListeners();
}