import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:hdp/src/helpers/helper.dart';
import 'package:hdp/src/models/hadith.dart';
import 'package:hdp/src/models/user.dart';
import 'package:http_parser/http_parser.dart' as parser;
import 'package:http/http.dart' as http;
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;


Future<Stream<Hadith>> getAllHadiths({var lastPage}) async {
  String url = '${GlobalConfiguration().getString('api_base_url')}post/all';

  if(lastPage != null ) {
    url += '?next=$lastPage';
  }

  final client = new http.Client();
  final streamedRest =await client.send(http.Request('get', Uri.parse(url)));
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPostsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Hadith.fromJson(data);
  });
}

Future<Stream<Hadith>> getHadithsByCategory(var catId, {var lastPage}) async {

  String url = '${GlobalConfiguration().getString('api_base_url')}post/category?category=${catId}';

  if(lastPage != null ) {
    url += '?next=$lastPage';
  }

  final client = new http.Client();
  final streamedRest =await client.send(http.Request('get', Uri.parse(url)));
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPostsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Hadith.fromJson(data);
  });

}


Future<Hadith> getPinned() async {
  Hadith _hadith;
  final String url = '${GlobalConfiguration().getString('api_base_url')}post/pinned';
  final client = new http.Client();
  final response = await client.get(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'}
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if (json.decode(body)['post'] != null) {
      _hadith = Hadith.fromJson(json.decode(body)['post']);
    }
  }
  return _hadith;
}