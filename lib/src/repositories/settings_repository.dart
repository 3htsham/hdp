import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hdp/config/strings.dart';
import 'package:hdp/src/models/settings.dart';
import 'package:hdp/src/models/user.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:global_configuration/global_configuration.dart';
import 'package:hdp/src/models/about.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

ValueNotifier<Settings> setting = new ValueNotifier(new Settings());

Future<Settings> initSettings() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if(prefs.containsKey(IS_DARK)) {
    bool isDark = await prefs.get(IS_DARK);
    setting.value.brightness.value = isDark ? Brightness.dark : Brightness.light;
    setting.value.placeholder.value = isDark ? "assets/img/placeholders/dark_placeholder.png" : "assets/img/placeholders/light_placeholder.png";
  }

  if(prefs.containsKey(APP_LANG)) {
    String langCode = await prefs.get(APP_LANG);
    setting.value.language.value = Locale(langCode, '');
  }

  if(prefs.containsKey(HADITH_NOTIF)) {
    bool hadithNotif = await prefs.get(HADITH_NOTIF);
    setting.value.hadithNotif.value = hadithNotif;
  } else {
    prefs.setBool(HADITH_NOTIF, true);
    setting.value.hadithNotif.value = true;
  }

  if(prefs.containsKey(POST_NOTIF)) {
    bool postNotif = await prefs.get(POST_NOTIF);
    setting.value.postNotif.value = postNotif;
  } else {
    prefs.setBool(POST_NOTIF, true);
    setting.value.postNotif.value = true;
  }

  if(prefs.containsKey(OTHER_NOTIF)) {
    bool otherNotif = await prefs.get(OTHER_NOTIF);
    setting.value.otherNotif.value = otherNotif;
  } else {
    prefs.setBool(OTHER_NOTIF, true);
    setting.value.otherNotif.value = true;
  }

  if (prefs.containsKey(ABOUT)) {
    setting.value.about.value = About.fromJson(json.decode(await prefs.get(ABOUT)));
  }

  if(prefs.containsKey(CALCULATION_METHOD_INDEX)) {
    setting.value.salahCalculationMethod.value = Settings.calculationMethods[prefs.getInt(CALCULATION_METHOD_INDEX)];
  }

  if(prefs.containsKey(ASR_CALCULATION)) {
    setting.value.salahAsrCalculation.value = Settings.asrCalculationMethods[prefs.getInt(ASR_CALCULATION)];
  }

  setting.notifyListeners();

  getAboutUs();

  return setting.value;

}

void toggleTheme() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isDark = setting.value.brightness.value == Brightness.dark ? true : false;
  prefs.setBool(IS_DARK, !isDark);
  setting.value.brightness.value = setting.value.brightness.value == Brightness.dark ? Brightness.light : Brightness.dark;
  setting.value.placeholder.value = !isDark ? "assets/img/dark_placeholder.png" : "assets/img/light_placeholder.png";
  setting.notifyListeners();
}

changeLang(String langCode) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(APP_LANG, langCode);
  setting.value.language.value = Locale(langCode, '');
  setting.notifyListeners();
}

void toggleHadithNotification(bool isOn) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(HADITH_NOTIF, isOn);
  setting.value.hadithNotif.value = isOn;
  setting.notifyListeners();
}

void togglePostNotification(bool isOn) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(POST_NOTIF, isOn);
  setting.value.postNotif.value = isOn;
  setting.notifyListeners();
}

void toggleOtherNotification(bool isOn) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(OTHER_NOTIF, isOn);
  setting.value.otherNotif.value = isOn;
  setting.notifyListeners();
}

getAboutUs() async {
  About about;
  final String url = '${GlobalConfiguration().getString('base_url')}hdp/about';
  final client = new http.Client();
  final response = await client.get(
    Uri.parse(url),
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if (json.decode(body)['about'] != null) {
      setAbout(body);
      setting.value.about.value = About.fromJson(json.decode(body)['about']);
      setting.notifyListeners();
    }
  } else {
    print(response.body);
  }
}

void setAbout(jsonString) async {
  if (json.decode(jsonString)['about'] != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(ABOUT, json.encode(json.decode(jsonString)['about']));
  }
}

Future<About> getAbout() async {
  About about;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(ABOUT)) {
    about = About.fromJson(json.decode(await prefs.get(ABOUT)));
  }
  return about;
}

///These will return Indexes
setCalculationMethod(int index) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt(CALCULATION_METHOD_INDEX, index);
  setting.value.salahCalculationMethod.value = Settings.calculationMethods[index];
  setting.notifyListeners();
}
setAsrCalculation(int index) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt(ASR_CALCULATION, index);
  setting.value.salahAsrCalculation.value = Settings.asrCalculationMethods[index];
  setting.notifyListeners();
}


/// Notifications Handling Stuff
Future<void> myBackgroundMessageHandler(Map<String, dynamic> message) async {

  print("onMessage: " + message.toString());

}