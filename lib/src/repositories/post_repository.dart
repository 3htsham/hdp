import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:hdp/src/helpers/helper.dart';
import 'package:hdp/src/models/post.dart';
import 'package:hdp/src/models/user.dart';
import 'package:http_parser/http_parser.dart' as parser;
import 'package:http/http.dart' as http;
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;

Future<Stream<Post>> addPost(Post _post) async {
  User currentUser = settingsRepo.setting.value.currentUser.value;
  final String apiToken = "?apiToken=${currentUser.apiToken}";

  final String url = '${GlobalConfiguration().getString('api_base_url')}other';

  Map<String, String> bodyMap = _post.toJson();
  bodyMap['apiToken'] = currentUser.apiToken;

  http.MultipartRequest request = http.MultipartRequest("post", Uri.parse(url));

  request.fields.addAll(bodyMap);
  if(_post.isMedia) {
    var file = await http.MultipartFile.fromPath(
        "mediaFile", _post.newImageIdentifier, filename: _post.newImageName,
        contentType: parser.MediaType.parse("application/octet-stream"));
    request.files.add(file);
  }
  print("Added");

  var response = await request.send();
  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPostData(data))
  // .expand((element) => element)
      .map((data) {
    var d = data;
    print(data);
    return Post.fromJson(data);
  }
  );
}

Future<Stream<Post>> getAllPosts({var lastPage}) async {
  String url = '${GlobalConfiguration().getString('api_base_url')}other/all';

  if(lastPage != null ) {
    url += '?next=$lastPage';
  }

  final client = new http.Client();
  final streamedRest =await client.send(http.Request('get', Uri.parse(url)));
  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPostsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Post.fromJson(data);
  });
}


Future<Stream<Post>> updatePost(Post post) async {
  User currentUser = settingsRepo.setting.value.currentUser.value;
  final String apiToken = "?apiToken=${currentUser.apiToken}";
  post.userId = currentUser.id;

  final String url = '${GlobalConfiguration().getString('api_base_url')}other';

  Map<String, String> bodyMap = post.toJson();
  bodyMap['apiToken'] = currentUser.apiToken;


  http.MultipartRequest request = http.MultipartRequest("put", Uri.parse(url));


  if(post.isMedia) {
    bodyMap['mediaLink'] = post.mediaLink;
  } else if(post.isNewMedia) {
    var file = await http.MultipartFile.fromPath(
        "mediaFile", post.newImageIdentifier, filename: post.newImageName,
        contentType: parser.MediaType.parse("application/octet-stream"));

    bodyMap['isMedia'] = post.isNewMedia.toString();
    bodyMap['isImage'] = post.isNewImage.toString();

    request.files.add(file);
  } else {
    bodyMap['mediaLink'] = "";
  }

  request.fields.addAll(bodyMap);


  var response = await request.send();
  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPostData(data))
  // .expand((element) => element)
      .map((data) {
    var d = data;
    print(data);
    return Post.fromJson(data);
  }
  );
}

Future<bool> deletePost(Post _post) async {
  bool isDeleted = false;
  User currentUser = settingsRepo.setting.value.currentUser.value;
  final String apiToken = "?apiToken=${currentUser.apiToken}";
  final String id = "&id=${_post.id}";

  final String url = '${GlobalConfiguration().getString('api_base_url')}other$apiToken$id';
  final client = new http.Client();
  final response = await client.delete(
      Uri.parse(url),
      headers: {HttpHeaders.contentTypeHeader: 'application/json'}
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['message'] != null) {
      isDeleted = json.decode(body)['message'].toString().toLowerCase() == "success";
    }
  }
  return isDeleted;
}