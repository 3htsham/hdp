class Helper {
  static getBannersData(Map<String, dynamic> data) {
    return data['banners'] ?? [];
  }

  static getBannerData(Map<String, dynamic> data) {
    return data['banner'] ?? [];
  }

  static getCategoriesData(Map<String, dynamic> data) {
    return data['categories'] ?? [];
  }
  static getPostData(Map<String, dynamic> data) {
    return data['post'] ?? [];
  }
  static getPostsData(Map<String, dynamic> data) {
    var newData = data['posts'];
    if(newData != null) {
      newData.forEach((d) {
        d['last_page'] = data['last_page'];
      });
    }
    // return data['posts'] ?? [];
    return newData;
  }
}