import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../generated/i18n.dart';

class UIHelper {
  static showAlertDialog(BuildContext context, String title, String content, VoidCallback onOk) {
    showCupertinoDialog(context: context, builder: (context) {
      return AlertDialog(
        actions: [
          TextButton(onPressed: (){
            onOk?.call();
          }, child: Text(S.of(context).ok
            , style: Theme.of(context).textTheme.bodyText1,))
        ],
        title: Text(title, style: Theme.of(context).textTheme.subtitle1),
        content: Text(content, style: Theme.of(context).textTheme.bodyText2,),
      );
    });
  }
}