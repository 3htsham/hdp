class Hadith {

  var id;
  var userId;
  var english;
  var urdu;
  bool isMedia = false;
  bool isImage = false;
  var whatsAppLink;
  var instagramLink;
  var youtubeLink;
  var telegramLink;
  var phone;
  bool isPinned = false;
  var time;
  var fileName;
  var mediaLink;
  bool isEdited = false;
  var categoryId;

  var newImageIdentifier; ///Media Identifier
  var newImageName; /// Media name
  bool isNewImage = false; ///If Hadith has new media as an image
  bool isNewMedia= false; ///If edited Hadith has new Media

  var lastPage;

  Hadith();

  Hadith.fromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.userId = data['userId'];
    this.english = data['english'];
    this.urdu = data['urdu'];
    this.isMedia = data['isMedia'] == "true" || data['isMedia'] == true;
    this.isImage = data['isImage'] == "true" || data['isImage'] == true;
    this.whatsAppLink = data['whatsAppLink'];
    this.instagramLink = data['instagramLink'];
    this.youtubeLink = data['youtubeLink'];
    this.telegramLink = data['telegramLink'];
    this.phone = data['phone'];
    this.isPinned = data['isPinned'] == "true" || data['isPinned'] == true;
    this.time = data['time'];
    this.fileName = data['fileName'];
    this.mediaLink = data['mediaLink'];
    // this.isEdited = data['isEdited'];
    this.categoryId = data['categoryId'];
    this.lastPage = data['last_page'] ?? "";
  }

  Map<String, String> toJson() {
    Map<String, String> data = Map<String, String>();
    data['id'] = this.id.toString();
    data['userId'] = this.userId.toString();
    data['english'] = this.english.toString();
    data['urdu'] = this.urdu.toString();
    data['isMedia'] = this.isMedia.toString();
    data['isImage'] = this.isImage.toString();
    data['whatsAppLink'] = this.whatsAppLink.toString();
    data['instagramLink'] = this.instagramLink.toString();
    data['youtubeLink'] = this.youtubeLink.toString();
    data['telegramLink'] = this.telegramLink.toString();
    data['phone'] = this.phone.toString();
    data['isPinned'] = this.isPinned.toString();
    data['time'] = this.time != null ? this.time.toString() : DateTime.now().millisecondsSinceEpoch.toString();
    data['fileName'] = this.fileName.toString();
    this.isMedia ? data['mediaLink'] = this.mediaLink.toString() : null;
    data['isEdited'] = this.isEdited.toString();
    data['categoryId'] = this.categoryId.toString();
    return data;
  }


}