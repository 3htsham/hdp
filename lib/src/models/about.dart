class About {

  String about;
  String contact;
  String whatsapp;
  String email;
  String playStoreUrl;

  About();

  About.fromJson(Map<String, dynamic> json) {
    about = json['about'];
    contact = json['contact'];
    whatsapp = json['whatsapp'];
    email = json['email'];
    playStoreUrl = json['playStoreUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['about'] = this.about;
    data['contact'] = this.contact;
    data['whatsapp'] = this.whatsapp;
    data['email'] = this.email;
    data['playStoreUrl'] = this.playStoreUrl;
    return data;
  }

}