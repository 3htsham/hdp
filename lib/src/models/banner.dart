class MyBanner {
  /*
    Link Types should be
        0 - None
        1 - Website
        2 - Instagram
        3 - Facebook
        4 - Play store
        5 - YouTube
    */

  var id;
  var image;
  var linkType = "0";
  var link;
  var time;
  bool enabled = true;
  var userId;
  var text;

  var newImageIdentifier;
  bool isNewImage = false;
  var newImageName;

  MyBanner();

  MyBanner.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.image = json['image'];
    this.linkType = json['linkType'];
    this.link = json['link'];
    this.time = json['time'];
    this.enabled = json['enabled'].toString().toLowerCase() == 'true';
    this.userId = json['userId'];
    this.text = json['text'];
  }

  Map<String, String> toJson() {
    Map<String, String> data = Map<String, String>();
    this.id != null ? data['id'] = this.id.toString() : null;
    this.image != null ? data['image'] = this.image.toString() : null;
    this.linkType != null ? data['linkType'] = this.linkType.toString() : null;
    this.link != null ? data['link'] = this.link.toString() : null;
    this.enabled != null ? data['enabled'] = this.enabled.toString() : null;
    this.text != null ? data['text'] = this.text.toString() : null;
    return data;
  }

}

class BannerLinkType {
/*
    Link Types should be
        0 - None
        1 - Website
        2 - Instagram
        3 - Facebook
        4 - Play store
        5 - YouTube
    */

  String name;
  String code;

  BannerLinkType({this.name, this.code});

}