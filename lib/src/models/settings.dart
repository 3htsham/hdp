import 'package:adhan/adhan.dart';
import 'package:flutter/material.dart';
import 'package:hdp/src/models/user.dart';
import 'package:hdp/src/models/about.dart';

class Settings {

  ValueNotifier<Locale> language = new ValueNotifier(Locale('en', ''));

  ValueNotifier<Brightness> brightness = new ValueNotifier(Brightness.light);
  ///For Dark: assets/img/dark_placeholder.png
  ///For Light: assets/img/light_placeholder.png
  ValueNotifier<String> placeholder = new ValueNotifier("assets/img/placeholders/light_placeholder.png");

  ValueNotifier<bool> hadithNotif = new ValueNotifier(true);
  ValueNotifier<bool> postNotif = new ValueNotifier(true);
  ValueNotifier<bool> otherNotif = new ValueNotifier(true);

  ValueNotifier<User> currentUser = new ValueNotifier(User());
  ValueNotifier<About> about = new ValueNotifier(About());

  ValueNotifier<CalculationMethod> salahCalculationMethod = new ValueNotifier(CalculationMethod.karachi);
  ValueNotifier<Madhab> salahAsrCalculation = new ValueNotifier(Madhab.hanafi);

  Settings();


  static List<CalculationMethod> calculationMethods = [
    CalculationMethod.karachi,
    CalculationMethod.dubai,
    CalculationMethod.tehran,
    CalculationMethod.umm_al_qura,
    CalculationMethod.moon_sighting_committee,
    CalculationMethod.qatar,
    CalculationMethod.kuwait,
    CalculationMethod.turkey,
    CalculationMethod.singapore,
    CalculationMethod.egyptian,
    CalculationMethod.muslim_world_league,
    CalculationMethod.north_america,
    CalculationMethod.other,
  ];
  static List<Madhab> asrCalculationMethods = [
    Madhab.hanafi,
    Madhab.shafi,
  ];

}