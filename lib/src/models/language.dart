import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;

class Language {
  String code;
  String englishName;
  String localName;
  IconData flag;
  bool selected;

  Language(this.code, this.englishName, this.localName, this.flag, {this.selected = false});
}

class LanguagesList {
  List<Language> _languages;

  LanguagesList() {
    this._languages = [
      new Language("en", "English", "English", Icons.translate, selected: settingRepo.setting.value.language.value.languageCode=="en",),
      new Language("ur", "Urdu", "اردو", Icons.translate, selected: settingRepo.setting.value.language.value.languageCode=="ur",),
    ];
  }

  List<Language> get languages => _languages;
}