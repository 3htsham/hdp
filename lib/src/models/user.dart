class User {

  String location;
  String image = "";

  var id;
  var name;
  var email;
  var password;
  var apiToken;

  User({this.name, this.image, this.location});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    password = json['password'];
    apiToken = json['api_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    this.password != null ? data['password'] = this.password : null;
    data['api_token'] = this.apiToken;
    return data;
  }

}