class Post {
  var id;
  var userId;
  var content;
  var isMedia = false;
  var isImage = false;
  var time;
  var fileName;
  var mediaLink;

  var newImageIdentifier; ///Media Identifier
  var newImageName; /// Media name
  bool isNewImage = false; ///If Post has new media as an image
  bool isNewMedia= false; ///If edited Post has new Media

  var lastPage;

  Post();

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    content = json['content'];
    isMedia = json['isMedia'] == true || json['isMedia'] == 'true';
    isImage = json['isImage'] == 'true' || json['isImage'] == true;
    time = json['time'];
    fileName = json['fileName'];
    mediaLink = json['mediaLink'];
    this.lastPage = json['last_page'] ?? "";
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    this.id != null ? data['id'] = this.id.toString() : null;
    data['content'] = this.content.toString();
    data['isImage'] = this.isImage.toString();
    return data;
  }
}