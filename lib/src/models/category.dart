import 'package:flutter/material.dart';

class Category {

  var id;
  var name;
  Color color;

  Category();

  Category.fromJson(Map<String, dynamic> data) {
    this.id = data['id'];
    this.name = data['name'];
  }

  Map<String, String> toJson() {
    Map<String, String> data = Map<String, String>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }

}