import 'package:hdp/src/models/banner.dart';
import 'package:hdp/src/models/category.dart';
import 'package:hdp/src/models/hadith.dart';
import 'package:hdp/src/models/post.dart';


class RouteArgument {

  MyBanner banner;

  int currentTab;
  String title;

  Hadith hadith;

  Post post;
  Category category;


  RouteArgument({
        this.currentTab,
        this.title,
        this.banner,
        this.hadith,
        this.post,
        this.category,
      });

}