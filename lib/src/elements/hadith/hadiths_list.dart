import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/home_controller.dart';
import 'package:hdp/src/elements/shimmer/hadith_shimmer.dart';
import 'package:hdp/src/elements/hadith/hadith_post.dart';
import 'package:hdp/src/models/route_argument.dart';


class HadithsList extends StatelessWidget {

  HomeController con;

  HadithsList({@required this.con});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
      child: con.isLoadingHadiths
          ? HadithShimmer()
          : con.hadiths.isEmpty
          ? SizedBox(height:0)
          : Column(
        children: [

          ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: con.hadiths.length,
            itemBuilder: (context, i) {

              return HadithPostWidget(
                hadith: con.hadiths[i],
                onLinkTop: (String link) {
                  con.launchLink(link);
                },
                onDownload: (){
                  if(con.hadiths[i].isMedia) {
                    con.downloadHadith(con.hadiths[i], !con.hadiths[i].isImage);
                  }
                },
                onHadithTap: (){
                  Navigator.of(context).pushNamed("/ViewHadith", arguments: RouteArgument(hadith: con.hadiths[i]));
                },
                onShare: (){
                  con.shareHadith(con.hadiths[i]);
                },
              );

            },
          ),

          con.lastPage != null
              ? con.isLoadingHadiths
              ? HadithShimmer()
              : Container(
            margin:
            EdgeInsets.symmetric(vertical: 7),
            child: InkWell(
              onTap: () {
                if (!con.isLoadingHadiths) {
                  con.getHadiths();
                }
              },
              splashColor: theme.accentColor,
              child: Container(
                width: size.width,
                height: 40,
                child: Center(
                    child: Text(
                      S.of(context).load_more,
                      style: textTheme.caption,
                    )),
              ),
            ),
          )
              : SizedBox(
            height: 0,
            width: 0,
          ),
        ],
      ),

    );

  }
}
