import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as material;
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/models/hadith.dart';
import 'package:hdp/config/app_config.dart' as config;
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:intl/intl.dart';

class HadithPostWidget extends StatelessWidget {

  Hadith hadith;
  VoidCallback onHadithTap;
  VoidCallback onShare;
  VoidCallback onDownload;
  Function(String) onLinkTop;
  bool isPinned = false;

  HadithPostWidget({
    @required this.hadith,
    this.onShare,
    this.onHadithTap,
    this.onDownload,
    this.isPinned = false,
    @required this.onLinkTop
  });

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    var dateTime = DateTime.fromMillisecondsSinceEpoch(hadith.time);
    var format = DateFormat("dd/MM/yyyy");
    var datePosted = format.format(dateTime);

    return InkWell(
      onTap: this.onHadithTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7),
        decoration: BoxDecoration(
            color: theme.scaffoldBackgroundColor,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: theme.focusColor,
                  blurRadius: 1,
                  spreadRadius: -1
              ),
            ]
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  children: [

                    this.isPinned
                        ? SizedBox(width: 25, child: Icon(CupertinoIcons.star_circle, color: config.Colors().pink2, size: 20,))
                        : SizedBox(width: 0, height: 0),

                    Text(
                        this.isPinned ? S.of(context).todays_hadith : datePosted ?? "",
                        style: textTheme.bodyText2
                    ),

                    Spacer(),

                    hadith.isMedia
                        ? IconButton(
                            onPressed: this.onDownload,
                            icon: Icon(
                              Icons.download_sharp,
                              size: 14,
                              color: config.Colors().green,
                            ),
                          )
                        : SizedBox(height: 0, width: 0),


                    IconButton(
                      onPressed: this.onShare,
                      icon: Icon(Icons.share, size: 14, color: config.Colors().orange,),
                    ),
                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Column(
                  children: [

                    Container(
                      width: size.width,
                      child: Text(hadith.urdu,
                        maxLines: 5,
                        overflow: TextOverflow.ellipsis,
                        style: textTheme.bodyText1,
                        textAlign: TextAlign.right,
                        textDirection: material.TextDirection.rtl,
                      ),
                    ),

                    SizedBox(height: 5,),

                    Container(
                      width: size.width,
                      child: Text(hadith.english,
                        maxLines: 5,
                        overflow: TextOverflow.ellipsis,
                        style: textTheme.bodyText1,
                        textAlign: TextAlign.left,
                        textDirection: material.TextDirection.ltr,
                      ),
                    ),


                    hadith.telegramLink.toString().length > 1
                        ? Container(
                      margin: EdgeInsets.only(top: 10),
                          child: InkWell(
                      onTap: (){this.onLinkTop(hadith.telegramLink.toString());},
                      child: Row(
                          children: [
                            Transform.rotate(
                                angle: -45,
                                child: Icon(Icons.send, size: 14, color: config.Colors().blue,)),
                            SizedBox(width: 5,),
                            Text(hadith.telegramLink, style: textTheme.bodyText2.merge(TextStyle(color: config.Colors().blue)),)
                          ],
                      ),
                    ),
                        ) : SizedBox(height: 0),

                    hadith.whatsAppLink.toString().length > 1
                        ? Container(
                      margin: EdgeInsets.only(top: 5),
                          child: InkWell(
                      onTap: (){ this.onLinkTop(hadith.whatsAppLink.toString()); },
                      child: Row(
                          children: [
                            Icon(CupertinoIcons.phone_circle, size: 14, color: config.Colors().green,),
                            SizedBox(width: 5,),
                            Text(hadith.whatsAppLink, style: textTheme.bodyText2.merge(TextStyle(color: config.Colors().green)),)
                          ],
                      ),
                    ),
                        ) : SizedBox(height: 0),

                    hadith.youtubeLink.toString().length > 1
                        ? Container(
                      margin: EdgeInsets.only(top: 5),
                          child: InkWell(
                      onTap: (){
                          this.onLinkTop(hadith.youtubeLink.toString());
                      },
                      child: Row(
                          children: [
                            ImageIcon(AssetImage("assets/icons/youtube.png"), size: 14, color: Colors.red,),
                            SizedBox(width: 5,),
                            Text(hadith.youtubeLink, style: textTheme.bodyText2.merge(TextStyle(color: Colors.red)),)
                          ],
                      ),
                    ),
                        ) : SizedBox(height: 0),

                    hadith.instagramLink.toString().length > 1
                        ? Container(
                      margin: EdgeInsets.only(top: 5),
                          child: InkWell(
                      onTap: () {
                          this.onLinkTop(hadith.instagramLink.toString());
                      },
                      child: Row(
                          children: [
                            ImageIcon(AssetImage("assets/icons/insta.png"), size: 14, color: Colors.redAccent,),
                            SizedBox(width: 5,),
                            Text(hadith.instagramLink, style: textTheme.bodyText2.merge(TextStyle(color: Colors.redAccent)),)
                          ],
                      ),
                    ),
                        ) : SizedBox(height: 0),

                  ],
                ),
              ),

              hadith.isMedia
                  ? hadith.isImage
                  ? Container(
                  width: size.width,
                  constraints: BoxConstraints(
                      maxHeight: size.width
                  ),
                  color: settingsRepo.setting.value.brightness.value == Brightness.dark
                      ? Colors.black
                      : Colors.white,
                  child: CachedNetworkImage(
                      imageUrl: hadith.mediaLink,
                    fit: BoxFit.cover,
                    placeholder: (context, value) {
                        return Image.asset(settingsRepo.setting.value.placeholder.value, fit: BoxFit.cover,);
                    },
                  )
              )
                  :
              // SizedBox(height: 0)
              BetterPlayer.network(
                hadith.mediaLink,
                // "https://storage.googleapis.com/hadithproduction-80938.appspot.com/hadiths/videos/1616334626138_image_picker18083158.mp4?GoogleAccessId=firebase-adminsdk-bp6ex%40hadithproduction-80938.iam.gserviceaccount.com&Expires=2524608000&Signature=CVa1%2B9JJXu0hsgA5LIAjean2X8p6iBdkAecBglP%2BOeuIh4goxNw5y40tSKuF2b5nXlUqWRyRQezUB1VCW6G5KyXGbmxkc%2BDJvuQl0lkozEt%2Bf1JyWWfXM1FGgqNXcDPBmQkK%2FZD1fYJx3YGxvPXdJBY1wGZ2VPH4wE7xnW4oIeyupZ6YMVSpaCu7ti40UjYp25yNP23CfILjwS%2FXCU%2FJGelTaavQe950%2Fdz7AGzTmMpDViRmXKUT103xgOGAPl1OiJqUtvsvqM3QTSstXouLW9G3rfVCSlsm6%2Bmx9W2zIOZzm0%2BXY7kcdh28Cxmt8ajwR55DGdWZuNDKd%2Br5PPd0kQ%3D%3D",
                betterPlayerConfiguration: BetterPlayerConfiguration(
                    fit: BoxFit.scaleDown
                ),
              )
                  : SizedBox(height: 0),



            ],
          ),
        ),
      ),
    );

  }
}
