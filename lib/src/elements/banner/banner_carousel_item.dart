import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hdp/src/models/banner.dart';
import 'package:intl/intl.dart';


class BannerCarouselItem extends StatelessWidget {

  MyBanner banner;
  VoidCallback onTap;

  BannerCarouselItem({@required this.banner, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    DateTime date = DateTime.fromMillisecondsSinceEpoch(banner.time);
    var format = DateFormat('dd MMM, yyyy');
    var dateTime = format.format(date);

    return InkWell(
      onTap: this.onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: CachedNetworkImage(
          imageUrl: banner.image,
          imageBuilder: (context, provider) {
            return Container(
              height: size.width * 0.526,
              width: size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: provider, fit: BoxFit.cover),
              ),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Colors.black.withOpacity(0),
                        Colors.black.withOpacity(0.3),
                        Colors.black.withOpacity(0.7),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    )),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: [
                        Row(),
                        Text(
                          banner.text,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: textTheme.caption.merge(
                              TextStyle(color: Colors.white)),
                        ),
                        Text(
                          dateTime,
                          style: textTheme.caption,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
          placeholder: (context, url) => Image.asset(
              "assets/img/placeholders/dark_placeholder.png"),
        ),
      ),
    );

  }
}
