import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:hdp/src/controllers/home_controller.dart';
import 'package:intl/intl.dart';
import 'package:hdp/src/elements/shimmer/banner_shimmer.dart';
import 'package:hdp/src/elements/banner/banner_carousel_item.dart';
import 'package:url_launcher/url_launcher.dart';

class BannerCarouselSlider extends StatelessWidget {
  HomeController con;

  BannerCarouselSlider({@required this.con});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: con.isLoadingBanners
          ? BannerShimmer()
          : con.banners.isEmpty
          ? SizedBox(
        height: 0,
      )
          : CarouselSlider(
        options: CarouselOptions(
            aspectRatio: 16 / 9,
            viewportFraction: 1,
            enableInfiniteScroll: true,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 10),
            autoPlayAnimationDuration: Duration(milliseconds: 500),
            autoPlayCurve: Curves.easeInOutSine,
            enlargeCenterPage: true,
            initialPage: 0
        ),
        items: con.banners.map((i) {
          DateTime date = DateTime.fromMillisecondsSinceEpoch(i.time);
          var format = DateFormat('dd MMM, yyyy');
          var dateTime = format.format(date);

          return BannerCarouselItem(
              banner: i,
              onTap: () async {
                if (i.link != null && i.link.toString().length > 4) {
                  var link = i.link;
                  if(!link.toString().startsWith("http") || !link.toString().startsWith("https")) {
                    link = "https://"+i.link.toString();
                  }
                  if (await canLaunch(link.toString())) {
                    launch(link.toString());
                  }
                }
              });
        }).toList(),
      )
    );
  }
}
