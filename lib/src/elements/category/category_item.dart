import 'package:flutter/material.dart';
import 'package:hdp/src/models/category.dart';

class CategoryItem extends StatelessWidget {

  Category cat;
  VoidCallback onTap;

  CategoryItem({@required this.cat, @required this.onTap});

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return InkWell(
      onTap: this.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 7),
        margin: EdgeInsets.symmetric(horizontal: 7),
        decoration: BoxDecoration(
          color: theme.scaffoldBackgroundColor,
          border: Border.all(width: 1, color: theme.highlightColor.withOpacity(0.5)),
          borderRadius: BorderRadius.circular(7),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset("assets/icons/book.png", color: cat.color,  height: 30,),
            SizedBox(width: 10,),
            Text(
              cat.name,
              style: textTheme.bodyText1,
            ),
            SizedBox(width: 5,),
          ],
        ),
      ),
    );
  }
}
