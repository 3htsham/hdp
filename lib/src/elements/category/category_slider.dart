import 'package:flutter/material.dart';
import 'package:hdp/src/controllers/home_controller.dart';
import 'package:hdp/src/elements/shimmer/category_shimmer.dart';
import 'package:hdp/src/elements/category/category_item.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;


class CategorySlider extends StatelessWidget {

  HomeController con;

  CategorySlider({@required this.con});

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.only(
        top: 10,
          left: settingsRepo.setting.value.language.value.languageCode == "en" ? 15 : 0,
          right: settingsRepo.setting.value.language.value.languageCode == "en" ? 0 : 15),
      child: con.isLoadingCategories
          ? CategoryShimmer()
          : con.categories.isEmpty
          ? SizedBox(height: 0)
          : Container(
        height: 50,
        padding: EdgeInsets.symmetric(vertical: 5),
        child: ListView.builder(
          primary: false,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: con.categories.length,
          itemBuilder: (context, index) {
            return CategoryItem(
              cat: con.categories[index],
              onTap: (){
                Navigator.of(context).pushNamed("/HadithsByCategory", arguments: RouteArgument(category: con.categories[index]));
                },
            );
          },
        ),
      ),
    );
  }
}
