import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class HadithShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Shimmer.fromColors(
        baseColor: theme.focusColor.withOpacity(0.5),
        highlightColor: theme.highlightColor.withOpacity(0.5),
        child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: 2,
          itemBuilder: (context, index) {
            return Container(
              // height: 150,
              width: size.width,
              margin: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                color: theme.highlightColor.withOpacity(0.3),
                borderRadius: BorderRadius.circular(10),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        children: [


                          Container(height: 8, width: 100, color: theme.highlightColor.withOpacity(0.5),),

                          Spacer(),

                          IconButton(
                            onPressed: (){},
                            icon: Icon(
                              Icons.download_sharp,
                              size: 14,
                              color: theme.highlightColor.withOpacity(0.5),
                            ),
                          ),

                          IconButton(
                            onPressed: (){},
                            icon: Icon(Icons.share, size: 14, color: theme.highlightColor.withOpacity(0.5),),
                          ),

                        ],
                      ),
                    ),

                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Column(
                        children: [

                          Container(
                              width: size.width,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    width: size.width,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),
                                  Container(
                                    width: size.width - 50,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),

                                  Container(
                                    width: size.width - 80,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),

                                  Container(
                                    width: 40,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),

                                ],
                              )
                          ),

                          SizedBox(height: 5,),

                          Container(
                              width: size.width,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: size.width,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),
                                  Container(
                                    width: size.width - 50,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),

                                  Container(
                                    width: size.width - 80,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),

                                  Container(
                                    width: 40,
                                    height: 10,
                                    margin: EdgeInsets.symmetric(vertical: 4),
                                    color: theme.highlightColor.withOpacity(0.3),
                                  ),

                                ],
                              )
                          ),

                          SizedBox(height: 10,),

                          Container(
                              width: size.width,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  Row(
                                    children: [
                                      Transform.rotate(
                                          angle: -45,
                                          child: Icon(Icons.send, size: 14, color: theme.highlightColor.withOpacity(0.5),)),
                                      SizedBox(width: 5,),
                                      Container(
                                        width: 135,
                                        height: 10,
                                        color: theme.highlightColor.withOpacity(0.3),
                                      )
                                    ],
                                  ),

                                  SizedBox(height: 5,),

                                  Row(
                                    children: [
                                      Icon(CupertinoIcons.phone_circle, size: 14, color: theme.highlightColor.withOpacity(0.5),),
                                      SizedBox(width: 5,),
                                      Container(
                                        width: 97,
                                        height: 10,
                                        color: theme.highlightColor.withOpacity(0.3),
                                      )
                                    ],
                                  ),

                                  SizedBox(height: 5,),

                                  Row(
                                    children: [
                                      ImageIcon(AssetImage("assets/icons/youtube.png"), size: 14, color: theme.highlightColor.withOpacity(0.5),),
                                      SizedBox(width: 5,),
                                      Container(
                                        width: 110,
                                        height: 10,
                                        color: theme.highlightColor.withOpacity(0.3),
                                      )
                                    ],
                                  ),

                                  SizedBox(height: 5,),

                                  Row(
                                    children: [
                                      ImageIcon(AssetImage("assets/icons/insta.png"), size: 14, color: theme.highlightColor.withOpacity(0.5),),
                                      SizedBox(width: 5,),
                                      Container(
                                        width: 123,
                                        height: 10,
                                        color: theme.highlightColor.withOpacity(0.3),
                                      )
                                    ],
                                  ),

                                ],
                              )
                          ),

                        ],
                      ),
                    ),

                    Container(
                      width: size.width,
                      height: size.width * 0.526,
                      constraints: BoxConstraints(
                          maxHeight: size.width
                      ),
                      color: theme.highlightColor.withOpacity(0.5),
                      child: Center(
                          child: Image.asset("assets/icons/alhadith_logo.png", height: 35, color: theme.highlightColor.withOpacity(0.5),)
                      ),
                    )

                  ],
                ),
              ),
            );
          },
        )
    );
  }
}

