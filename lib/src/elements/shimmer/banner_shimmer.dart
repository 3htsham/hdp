import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';


class BannerShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Shimmer.fromColors(
        baseColor: theme.focusColor.withOpacity(0.5),
        highlightColor: theme.highlightColor.withOpacity(0.5),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          width: size.width,
          height: size.width * 0.526,
          decoration: BoxDecoration(
            color: theme.highlightColor.withOpacity(0.5),
          ),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          height: 10,
                          width: 130,
                          color: theme.highlightColor,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                          height: 10,
                          width: 80,
                          color: theme.highlightColor,
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );

  }
}
