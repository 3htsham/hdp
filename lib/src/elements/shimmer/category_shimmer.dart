import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CategoryShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Container(
      height: 50,
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Shimmer.fromColors(
          baseColor: theme.focusColor.withOpacity(0.5),
          highlightColor: theme.highlightColor.withOpacity(0.5),
          child: ListView.builder(
            itemCount: 4,
            primary: false,
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Container(
                padding: EdgeInsets.symmetric(horizontal: 7),
                margin: EdgeInsets.symmetric(horizontal: 7),
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: theme.highlightColor.withOpacity(0.5)),
                    borderRadius: BorderRadius.circular(7)
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset("assets/icons/book.png", color: theme.highlightColor.withOpacity(0.5),  height: 30,),
                    SizedBox(width: 10,),
                    Container(
                      height: 10,
                      width: 80,
                      color: theme.highlightColor,
                    )
                  ],
                ),
              );
            },
          )
      ),
    );

  }
}
