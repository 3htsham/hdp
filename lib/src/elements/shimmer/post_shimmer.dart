import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';


class PostShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var size = MediaQuery.of(context).size;

    return Shimmer.fromColors(
      baseColor: theme.focusColor.withOpacity(0.5),
      highlightColor: theme.highlightColor.withOpacity(0.5),
      child:  ListView.builder(
        itemCount: 3,
        shrinkWrap: true,
        primary: false,
        itemBuilder: (context, i) {
          return Container(
            margin: EdgeInsets.symmetric(vertical: 5),
            decoration: BoxDecoration(
              color: theme.highlightColor.withOpacity(0.2),
              borderRadius: BorderRadius.circular(10),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                    child: Row(
                      children: [

                        Container(
                          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                          height: 10,
                          width: 80,
                          color: theme.highlightColor,
                        ),

                        Spacer(),

                      ],
                    ),
                  ),

                  Container(
                      width: size.width,
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size.width,
                            height: 10,
                            margin: EdgeInsets.symmetric(vertical: 4),
                            color: theme.highlightColor.withOpacity(0.3),
                          ),
                          Container(
                            width: size.width - 50,
                            height: 10,
                            margin: EdgeInsets.symmetric(vertical: 4),
                            color: theme.highlightColor.withOpacity(0.3),
                          ),

                          Container(
                            width: size.width - 80,
                            height: 10,
                            margin: EdgeInsets.symmetric(vertical: 4),
                            color: theme.highlightColor.withOpacity(0.3),
                          ),

                          Container(
                            width: 40,
                            height: 10,
                            margin: EdgeInsets.symmetric(vertical: 4),
                            color: theme.highlightColor.withOpacity(0.3),
                          ),

                        ],
                      )
                  ),

                  SizedBox(height: 10),

                  Container(
                    width: size.width,
                    height: size.width * 0.526,
                    constraints: BoxConstraints(
                        maxHeight: size.width
                    ),
                    color: theme.highlightColor.withOpacity(0.5),
                    child: Center(
                        child: Image.asset("assets/icons/alhadith_logo.png", height: 35, color: theme.highlightColor.withOpacity(0.5),)
                    ),
                  )



                ],
              ),
            ),
          );
        },
      )
    );
  }
}
