import 'package:better_player/better_player.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:hdp/src/models/post.dart';
import 'package:hdp/config/app_config.dart' as config;


class PostWidget extends StatelessWidget {

  Post post;
  VoidCallback onTap;

  PostWidget({
    @required this.onTap,
    @required this.post,
  });

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    var dateTime = DateTime.fromMillisecondsSinceEpoch(post.time);
    var format = DateFormat("dd/MM/yyyy");
    var datePosted = format.format(dateTime);


    return InkWell(
      onTap: this.onTap,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            color: theme.scaffoldBackgroundColor,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: theme.focusColor,
                  blurRadius: 1,
                  spreadRadius: -1
              ),
            ]
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                child: Row(
                  children: [

                    Text(
                        datePosted ?? "",
                        style: textTheme.bodyText2.merge(TextStyle(
                          color: textTheme.bodyText2.color.withOpacity(0.8)
                        ))
                    ),

                    Spacer(),

                  ],
                ),
              ),

              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Container(
                  width: size.width,
                  child: Text(
                    post.content,
                    maxLines: 5,
                    overflow: TextOverflow.ellipsis,
                    style: textTheme.bodyText1,
                  ),
                ),
              ),

              post.isMedia
                  ? post.isImage
                  ? Container(
                  width: size.width,
                  constraints: BoxConstraints(
                      maxHeight: size.width
                  ),
                  color: settingsRepo.setting.value.brightness.value == Brightness.dark
                      ? Colors.black
                      : Colors.white,
                  child: CachedNetworkImage(
                      imageUrl: post.mediaLink,
                      fit: BoxFit.cover,
                    placeholder: (context, value) {
                      return Image.asset(settingsRepo.setting.value.placeholder.value, fit: BoxFit.cover,);
                    },
                  )
              )
                  :
              // SizedBox(height: 0)
              BetterPlayer.network(
                post.mediaLink,
                // "https://storage.googleapis.com/hadithproduction-80938.appspot.com/hadiths/videos/1616334626138_image_picker18083158.mp4?GoogleAccessId=firebase-adminsdk-bp6ex%40hadithproduction-80938.iam.gserviceaccount.com&Expires=2524608000&Signature=CVa1%2B9JJXu0hsgA5LIAjean2X8p6iBdkAecBglP%2BOeuIh4goxNw5y40tSKuF2b5nXlUqWRyRQezUB1VCW6G5KyXGbmxkc%2BDJvuQl0lkozEt%2Bf1JyWWfXM1FGgqNXcDPBmQkK%2FZD1fYJx3YGxvPXdJBY1wGZ2VPH4wE7xnW4oIeyupZ6YMVSpaCu7ti40UjYp25yNP23CfILjwS%2FXCU%2FJGelTaavQe950%2Fdz7AGzTmMpDViRmXKUT103xgOGAPl1OiJqUtvsvqM3QTSstXouLW9G3rfVCSlsm6%2Bmx9W2zIOZzm0%2BXY7kcdh28Cxmt8ajwR55DGdWZuNDKd%2Br5PPd0kQ%3D%3D",
                betterPlayerConfiguration: BetterPlayerConfiguration(
                    fit: BoxFit.scaleDown
                ),
              )
                  : SizedBox(height: 0),



            ],
          ),
        ),
      ),
    );

  }
}
