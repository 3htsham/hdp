import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/controllers/post_controller.dart';
import 'package:hdp/src/elements/post/post_widget.dart';
import 'package:hdp/src/models/route_argument.dart';
import 'package:hdp/src/elements/shimmer/post_shimmer.dart';

class PostsListWidget extends StatelessWidget {

  PostController con;

  PostsListWidget({this.con});

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = Theme.of(context).textTheme;
    var size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: con.isLoadingPosts
          ?  PostShimmer()
          : Column(
            children: [
              ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: con.posts.length,
        itemBuilder: (context, index) {
              return PostWidget(
                post: con.posts[index],
                onTap: (){
                  Navigator.of(context).pushNamed("/ViewPost", arguments: RouteArgument(post: con.posts[index]));
                },
              );
        },
      ),

              con.lastPage != null
                  ? con.isLoading
                  ? PostShimmer()
                  : Container(
                margin: EdgeInsets.symmetric(vertical: 7),
                child: InkWell(
                  onTap: () {
                    if(!con.isLoading) {
                      con.getPosts();
                    }
                  },
                  splashColor: theme.accentColor,
                  child: Container(
                    width: size.width,
                    height: 40,
                    child: Center(
                        child: Text(
                          S.of(context).load_more,
                          style: textTheme.caption,
                        )),
                  ),
                ),
              )
                  : SizedBox(
                height: 0,
                width: 0,
              ),

            ],
          ),
    );

  }
}
