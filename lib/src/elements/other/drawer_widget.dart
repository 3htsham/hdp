import 'dart:ui';
import 'package:launch_review/launch_review.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/repositories/settings_repository.dart' as settingRepo;

class DrawerWidget extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Container(
      width: size.width * 0.7,
      child: Drawer(
        child: Column(
          children: [

            Container(
              child: Stack(
                children: [
                  Image.asset("assets/img/drawer_bg.png", ),

                  Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          children: [

                            Image.asset(
                              "assets/icons/white_moon_logo.png",
                              width: 100,
                            ),

                            SizedBox(height: 20,),

                            Text(S.of(context).appName, style: textTheme.headline5,),

                            Text("________", style: textTheme.caption,),

                            SizedBox(height: 20,),

                          ],
                        ),
                      ))
                ],
              ),
            ),

            ListTile(
                onTap: () {
                  Navigator.of(context).pushNamedAndRemoveUntil('/Pages', (Route<dynamic> route) => false, arguments: 2);
                },
                title: Text(S.of(context).home,
                  style: textTheme.headline6,
                ),
                leading: Icon(CupertinoIcons.home, size: 20,)
            ),

            ListTile(
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed('/Settings');
                },
                title: Text(S.of(context).settings,
                  style: textTheme.headline6,
                ),
                leading: Icon(CupertinoIcons.settings, size: 20,)
            ),

            ListTile(
              onTap: () {
                settingRepo.toggleTheme();
                Navigator.of(context).pushNamedAndRemoveUntil('/Pages', (Route<dynamic> route) => false, arguments: 2);
              },
              title: Text(
                settingRepo.setting.value.brightness.value == Brightness.dark
                    ? S.of(context).light_mode
                    : S.of(context).dark_mode,
                style: textTheme.headline6,
              ),
              leading: Icon(CupertinoIcons.brightness, size: 20,)
            ),

            ListTile(
                onTap: () {
                  Navigator.of(context).pushNamed('/About');
                },
                title: Text(S.of(context).about_us,
                  style: textTheme.headline6,
                ),
                leading: ImageIcon(
                  AssetImage("assets/icon_pack/about.png")
                )
            ),

            ListTile(
                onTap: () {
                  LaunchReview.launch();
                },
                title: Text(
                  S.of(context).rate,
                  style: textTheme.headline6,
                ),
                leading: Icon(CupertinoIcons.star, size: 20,)
            ),

          ],
        ),
      ),
    );

  }


}
