import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hdp/generated/i18n.dart';
import 'package:hdp/src/animations/widget_animator.dart';
import 'package:shimmer/shimmer.dart';

class LogoLoadingShimmer extends StatelessWidget {
  final String text;
  bool showText = true;
  LogoLoadingShimmer({this.text, this.showText});
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Shimmer.fromColors(
      baseColor: Colors.transparent,
      highlightColor: Theme.of(context).focusColor,
      enabled: true,
      child: Container(
        width: width,
        height: height,
        color: Colors.transparent,
        padding: EdgeInsets.all(8.0),
        child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/icons/alhadith_logo.png', height: height * 0.30),
                Image.asset('assets/icons/production.png', width: width * 0.20),
                SizedBox(height: 10),
                WidgetAnimator(
                    Text(
                        showText ? "${S.of(context).loading}...!" : " ",
                        style: Theme.of(context).textTheme.bodyText1))
              ],
            )),
      ),
    );
  }
}