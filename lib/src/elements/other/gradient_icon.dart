import 'package:flutter/material.dart';

class GradientIcon extends StatelessWidget {
  GradientIcon({
        this.icon, this.imageIconAsset,
        this.size,
        this.gradient,
      });

  final IconData icon;
  final String imageIconAsset;
  final double size;
  final Gradient gradient;
  bool isFa = false;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      child: SizedBox(
        width: size * 1.2,
        height: size * 1.2,
        child: this.icon != null
            ? Icon(
                icon,
                size: size,
                color: Colors.white,
              )
            : Image.asset(
                imageIconAsset,
                width: this.size,
                height: this.size,
                color: Colors.white,
              ),
      ),
      shaderCallback: (Rect bounds) {
        final Rect rect = Rect.fromLTRB(0, 0, size, size);
        return gradient.createShader(rect);
      },
    );
  }
}

///USE IT LIKE
/**


    GradientIcon(
      icon: Icons.add_alert, or imageIcon: 'assets/icon/iconName.png',
      size: 50.0,
      gradient: LinearGradient(
        colors: <Color>[
          Colors.red,
          Colors.yellow,
          Colors.blue,
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      ),
    )

 **/